#!/usr/bin/python

import json
import csv

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/img_key.json','r') as in_file:
    img_dat = json.load(in_file)

out_data = []

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMaseters/relion/proteasome_zoo/data/star_prep.tsv','r') as in_file:
    reader = csv.reader(in_file,delimiter='\t')

    for line in reader:
        img = img_dat[line[0]]

        y = 3810 - int(float(line[2])) * 6
        x = int(float(line[1])) * 6

        if x > 3710 or y > 3810:
            continue
        elif x < 0 or y < 0:
            continue
        
        line[0] = img
        line[1] = y
        line[2] = x
    
        out_data.append(line)

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMaseters/relion/proteasome_zoo/data/star_init.tsv','w') as out_file:
    writer = csv.writer(out_file,delimiter='\t')

    for line in out_data:
        writer.writerow(line)

