#!/usr/bin/python

import json
import os
import csv

from PIL import Image, ImageDraw

def main():
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json','r') as in_file:
        gold_conv = json.load(in_file)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/img_key.json','r') as in_file:
        img_dat = json.load(in_file)
        

        # voted data

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/template_data/data/template_data.tsv','r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')

        curr_id = None

        for line in reader:
            subj_id = line[0]
            if curr_id != subj_id and curr_id:
                draw_circles(curr_data,'/Users/Jake/Documents/Projects/crowdProj/ProteasomeLid/lid_jpg/%s' % curr_id,'/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/template_data/marked_imgs/%s' % curr_id)

                return
                # curr_data = [(int(float(line[2])/6),int((3838-float(line[1]))/6))]
                # curr_id = subj_id
            if curr_id != subj_id:
                curr_data = [(int(float(line[1])/6), 639-int(float(line[2])/6))]
                # curr_data = [(639-int((3810-float(line[1]))/6),int(float(line[2]/6)))]
                # curr_data = [(int(float(line[2])/6),639-int((3810-float(line[1]))/6))]
                curr_id = subj_id
            else:
                curr_data.append((int(float(line[1])/6), 639-int(float(line[2])/6)))
                # curr_data.append((639-int((3810-float(line[1]))/6),int(float(line[2]/6))))
                # curr_data.append((618-int(float(line[2])/6),639-int((3810-float(line[1]))/6)))
                
                

        # Start drawing here

        #Draw individual votes first

        # init_img = '/Users/Jake/Documents/Projects/crowdProj/ProteasomeLid/lid_jpg/%s' % img_dat[subj_dir]
        # indiv_votes = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/indiv_votes/%s.jpg' % subj_dir
        # draw_circles(anno_data,init_img,indiv_votes)

        # #Now voted

        # voted_img = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/voted_nogold/%s.jpg' % subj_dir
        # draw_votes(vote_data,init_img,voted_img)

        # gold_img = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/voted_wgold/%s.jpg' % subj_dir

        # if if_gold:
        #     draw_gold(gold_data,voted_img,gold_img)

        return

def draw_gold(points,filename, out_file):
    with Image.open(filename) as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        for point in points:
            drawer.ellipse([point[0]-4,point[1]-4,point[0]+4,point[1]+4],fill=None,outline=(255,255,51,255))
        del drawer
        img.save(out_file)

def draw_votes(points,img_file,out_file):
    with Image.open(img_file).convert("RGBA") as img:
        drawer = ImageDraw.Draw(img,"RGBA")

        # black for orphan votes, blue-red gradient to show anything above that
        
        color_code = {1: '#000000', 2: '#0000FF', 3: '#1500E9', 4: '#2A00D4', 5: '#3F00BF', 6: '#5500AA', 7: '#6A0094', 8: '#7F007F', 9: '#94006A', 10: '#AA0055', 11: '#BF003F', 12: '#D4002A', 13: '#E90015', 14: 'FF0000'}

        for point in points:
            drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],outline=color_code[point[2]])

        del drawer
        img.save(out_file)

def draw_circles(points,img_file,out_file):
    with Image.open(img_file).convert("RGB") as img:
        
        drawer = ImageDraw.Draw(img)

        colors = ['red','blue','orange','purple','pink','yellow','gray','lime','green','aqua','black','cyan','Brown','Olive']
        color = 'red'
        print len(points)
        for point in points:    
            print point
            drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],fill=None,outline=color)

        del drawer
        img.save(out_file)

    
if __name__ == "__main__":
    main()
