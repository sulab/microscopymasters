---
title: "Zooniverse Report"
output: html_document
---

```{r setup, echo=0}

library(knitr)
opts_knit$set(upload.fun = image_uri)

library(ggplot2)
library(reshape2)
library(gridExtra)

```

```{r Progress, echo=0, warning=0}

time_data <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/raw_data/picker_times.txt")
time_data$total <- 1:nrow(time_data)

unique_times <- data.frame()
for (date_ in unique(time_data$V1)){
  datemax <- max(subset(time_data,V1==date_)$total)

  unique_times <- rbind(unique_times,data.frame(Date = date_,total = datemax))
}

progress_plot <- ggplot(data=unique_times,aes(x=as.Date(Date),y=total)) + geom_bar(stat = "identity",aes(colour=total,fill=total)) +
    ggtitle("Microscopy Masters Progress") +
    xlab("Date") +
    ylab("Classifications") +
    guides(colour=FALSE,fill=FALSE) +
    theme(plot.title = element_text(size=20))



progress_plot

```

```{r Daily-Progress, echo=0, warning=0}

time_data <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/raw_data/picker_times.txt")

unique_times <- data.frame()

daily_plot <- ggplot(data=time_data,aes(x=as.Date(V1),colour=..count..,fill=..count..)) +
    geom_histogram(binwidth = 1) +
    xlab("Date") +
    ylab("Classifications") +
    guides(colour=FALSE,fill=FALSE) +
    ggtitle("Daily Classifications") +
    theme(plot.title = element_text(size=20))



daily_plot

```

```{r user-trellis, echo=0, message=0, warning=0,fig.width=10, fig.height=15}

zoo.data <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv",header=T,sep='\t')
zoo.data$x <- 0
zoo.data$user_id = as.factor(zoo.data$user_id)
zoo.data$precision <- apply(zoo.data[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.data$recall <- apply(zoo.data[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.data$precision[is.nan(zoo.data$precision)] <- 1
zoo.data$recall[is.nan(zoo.data$recall)] <- 1
zoo.data$fscore <- apply(zoo.data[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.data[zoo.data$tp==0,]$fscore <- 0
zoo.data <- subset(zoo.data,!is.na(user_id))

user.freq <- table(zoo.data$user_id)
user.freq <- user.freq[order(user.freq,decreasing=TRUE)]
user.freq <- user.freq[user.freq>1]

user.plots <- vector('list',length(user.freq)) ## + length(user.freq) %/% 10 + 1) 
cnt <- 0

for(i in names(user.freq)){
    cnt <- cnt+1
    user.frame <- subset(zoo.data,user_id==i)
    user.plot <- ggplot() + geom_point(data=user.frame,aes_string(x='x',y='fscore'),alpha=0.3) + xlab('') + ylab('') + ylim(0,1.0) + theme(axis.text.x = element_blank(),axis.ticks.x = element_blank()) + coord_fixed(ratio=5)

    if(cnt%%10 != 1){
        user.plot <- user.plot + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank())
    }
    ## if(cnt%%10 == 9){
    ##     user.plot <- user.plot + theme(
    ## }
    
    user.plots[[cnt]] <- user.plot
}

dotGrob <- marrangeGrob(user.plots,ncol=10,nrow=4,widths=c(1.27,rep(1,9)))
dotGrob

#grid.arrange(dotGrob)
## do.call('grid.arrange',c(user.plots,ncol=10))

```

```{r Fscore-voted-AMT-vs-Zoo, echo=0, warning=0, message=0}

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
anno.cnts <- table(crowd.data$subj_id)
cnt.df <- data.frame(anno.cnts)
cnt.df <- cnt.df[order(cnt.df$Var1),]

voted_perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/voted_performance.tsv",sep = "\t",header = T)
voted_perf$precision <- apply(voted_perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
voted_perf$recall <- apply(voted_perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
voted_perf$precision[is.nan(voted_perf$precision)] <- 1
voted_perf$recall[is.nan(voted_perf$recall)] <- 1
voted_perf$fscore <- apply(voted_perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
vote.data <- subset(voted_perf, v == 0)
vote.data <- vote.data[order(vote.data$subj_id),]
vote.data <- cbind(vote.data,cnt=cnt.df[,c("Freq")])
vote.data <- subset(vote.data,cnt>4)

turk.perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
mturk.perf <- melt(turk.perf,id=c("subj_id"))
cturk.perf <- dcast(mturk.perf,subj_id~variable,mean)
cturk.perf$precision <- apply(cturk.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
cturk.perf$recall <- apply(cturk.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
cturk.perf$precision[is.nan(cturk.perf$precision)] <- 1
cturk.perf$recall[is.nan(cturk.perf$recall)] <- 1
cturk.perf$fscore <- apply(cturk.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
#cturk.perf <- subset(turk.perf, v == 1 & dist == 10)

voted.hist <- ggplot() +
    geom_histogram(data=vote.data,aes(x=fscore,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=cturk.perf,aes(x=fscore,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Fscore after Voting") +
    scale_colour_manual(name="group", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="group", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='group',values=c("red", "blue"), labels=c("Zoo", "AMT"))
 
voted.hist

```

```{r agreement-thresholding, echo=0}

agree.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/agreement.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    agree.dat <- rbind(agree.dat,data)
}

perf.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/gold_perf.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    perf.dat <- rbind(perf.dat,data)
}

random.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/random_perf.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    random.dat <- rbind(random.dat,data)
}

perf.mdat <- melt(perf.dat[,c("tp","fp","fn","perc")],id=c("perc"))
perf.cdat <- dcast(perf.mdat,perc~variable,sum)
perf.cdat$precision <- apply(perf.cdat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
perf.cdat$recall <- apply(perf.cdat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
perf.cdat$fscore <- apply(perf.cdat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
perf.cdat$fscore[perf.cdat$tp==0] <- 0
perf.cdat$group <- 'Fscore'

random.mdat <- melt(random.dat[,c("tp","fp","fn","perc")],id=c("perc"))
random.cdat <- dcast(random.mdat,perc~variable,sum)
random.cdat$precision <- apply(random.cdat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
random.cdat$recall <- apply(random.cdat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
random.cdat$fscore <- apply(random.cdat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
random.cdat$fscore[random.cdat$tp==0] <- 0
random.cdat$group <- 'Randomized Fscore'

agree.dat$agreement <- apply(agree.dat[,c("intersection","union")],1,function(x) x[1]/x[2])
agree.dat$agreement[agree.dat$union==0] = 1
agree.mdat <- melt(agree.dat[,c("intersection","union","perc","agreement")],id=c("perc"))
agree.cdat <- dcast(agree.mdat, perc~variable,mean)
agree.cdat$group <- "Agreement"

agree.cdat$fscore <- agree.cdat$agreement

plot.dat <- rbind(agree.cdat[,c("perc","fscore","group")], perf.cdat[,c("perc","fscore","group")], random.cdat[,c("perc","fscore","group")])

## agree.plot <- ggplot() +
##     geom_point(data=agree.cdat,aes(x=perc,y=agreement,colour='blue'),size=2) +
##     geom_point(data=perf.cdat,aes(x=perc,y=fscore,colour='red'),size=2,alpha=0.4) +
##     geom_point(data=random.cdat,aes(x=perc,y=fscore,colour='green'),size=2,alpha=0.4) + 
##     ylim(0,1.0)

agree.plot <- ggplot() +
    geom_point(data=plot.dat,aes(x=perc,y=fscore,colour=group),size=3.5) +
    ylim(0,1.0) +
    xlab('Percent Removed') +
    ylab('Score') +
    ggtitle('User-based Thresholding') +
    theme(plot.title = element_text(size=20))

agree.plot

```

<!-- ```{r thresholding-lengths, echo=0} -->

<!-- indiv.dat <- data.frame() -->

<!-- for(i in 0:9){ -->
<!--     data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/%02d/indiv_gold.tsv',i*10),sep='\t',header=T) -->
<!--     data$perc <- i -->
<!--     indiv.dat <- rbind(indiv.dat,data) -->
<!-- } -->

<!-- indiv.rand <- data.frame() -->

<!-- for(i in 0:9){ -->
<!--     data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/%02d/indiv_rand.tsv',i*10),sep='\t',header=T) -->
<!--     data$perc <- i -->
<!--     indiv.rand <- rbind(indiv.rand,data) -->
<!-- } -->

<!-- indiv.dat$lengths <- apply(indiv.dat[,c("tp","fp")], 1, function(x) x[1]+x[2]) -->
<!-- indiv.mdat <- melt(indiv.dat[,c("tp","fp","fn","perc","lengths")],id=c("perc")) -->
<!-- indiv.cdat <- dcast(indiv.mdat,perc~variable,mean) -->
<!-- indiv.cdat$group <- "IR" -->

<!-- indiv.rand$lengths <- apply(indiv.rand[,c("tp","fp")], 1, function(x) x[1]+x[2]) -->
<!-- indiv.mrand <- melt(indiv.rand[,c("tp","fp","fn","perc","lengths")],id=c("perc")) -->
<!-- indiv.crand <- dcast(indiv.mrand,perc~variable,mean) -->
<!-- indiv.crand$group <- "Random" -->

<!-- plot.dat <- rbind(indiv.cdat,indiv.crand) -->

<!-- length.plot <- ggplot() + -->
<!--     geom_point(data = plot.dat, aes(x = perc, y = lengths,colour=group),size=2) -->

<!-- length.plot -->

<!-- ``` -->

Thresholding by individual annotations (opposed to users):

```{r agreement-thresholding-indiv, echo=0}

agree.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/agreement.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    agree.dat <- rbind(agree.dat,data)
}

perf.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/gold_perf.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    perf.dat <- rbind(perf.dat,data)
}

random.dat <- data.frame()

for(i in 0:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/random_perf.tsv',i*10),sep='\t',header=T)
    data$perc <- i
    random.dat <- rbind(random.dat,data)
}

perf.mdat <- melt(perf.dat[,c("tp","fp","fn","perc")],id=c("perc"))
perf.cdat <- dcast(perf.mdat,perc~variable,sum)
perf.cdat$precision <- apply(perf.cdat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
perf.cdat$recall <- apply(perf.cdat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
perf.cdat$fscore <- apply(perf.cdat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
perf.cdat$fscore[perf.cdat$tp==0] <- 0
perf.cdat$group <- 'Fscore'

random.mdat <- melt(random.dat[,c("tp","fp","fn","perc")],id=c("perc"))
random.cdat <- dcast(random.mdat,perc~variable,sum)
random.cdat$precision <- apply(random.cdat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
random.cdat$recall <- apply(random.cdat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
random.cdat$fscore <- apply(random.cdat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
random.cdat$fscore[random.cdat$tp==0] <- 0
random.cdat$group <- 'Randomized Fscore'

agree.dat$agreement <- apply(agree.dat[,c("intersection","union")],1,function(x) x[1]/x[2])
agree.dat$agreement[agree.dat$union==0] = 1
agree.mdat <- melt(agree.dat[,c("intersection","union","perc","agreement")],id=c("perc"))
agree.cdat <- dcast(agree.mdat, perc~variable,mean)
agree.cdat$group <- "Agreement"

agree.cdat$fscore <- agree.cdat$agreement

plot.dat <- rbind(agree.cdat[,c("perc","fscore","group")], perf.cdat[,c("perc","fscore","group")], random.cdat[,c("perc","fscore","group")])

## agree.plot <- ggplot() +
##     geom_point(data=agree.cdat,aes(x=perc,y=agreement,colour='blue'),size=2) +
##     geom_point(data=perf.cdat,aes(x=perc,y=fscore,colour='red'),size=2,alpha=0.4) +
##     geom_point(data=random.cdat,aes(x=perc,y=fscore,colour='green'),size=2,alpha=0.4) + 
##     ylim(0,1.0)

agree.plot <- ggplot() +
    geom_point(data=plot.dat,aes(x=perc,y=fscore,colour=group),size=3.5) +
    ylim(0,1.0) +
    xlab('Percent Removed') +
    ylab('Score') +
    ggtitle('User-based Thresholding') +
    theme(plot.title = element_text(size=20))

agree.plot

```

<!-- ```{r indiv-thresholding-lengths, echo=0} -->

<!-- indiv.dat <- data.frame() -->

<!-- for(i in 0:9){ -->
<!--     data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/indiv_gold.tsv',i*10),sep='\t',header=T) -->
<!--     data$perc <- i -->
<!--     indiv.dat <- rbind(indiv.dat,data) -->
<!-- } -->

<!-- indiv.rand <- data.frame() -->

<!-- for(i in 0:9){ -->
<!--     data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/indiv_rand.tsv',i*10),sep='\t',header=T) -->
<!--     data$perc <- i -->
<!--     indiv.rand <- rbind(indiv.rand,data) -->
<!-- } -->

<!-- indiv.dat$lengths <- apply(indiv.dat[,c("tp","fp")], 1, function(x) x[1]+x[2]) -->
<!-- indiv.mdat <- melt(indiv.dat[,c("tp","fp","fn","perc","lengths")],id=c("perc")) -->
<!-- indiv.cdat <- dcast(indiv.mdat,perc~variable,mean) -->
<!-- indiv.cdat$group <- "IR" -->

<!-- indiv.rand$lengths <- apply(indiv.rand[,c("tp","fp")], 1, function(x) x[1]+x[2]) -->
<!-- indiv.mrand <- melt(indiv.rand[,c("tp","fp","fn","perc","lengths")],id=c("perc")) -->
<!-- indiv.crand <- dcast(indiv.mrand,perc~variable,mean) -->
<!-- indiv.crand$group <- "Random" -->

<!-- plot.dat <- rbind(indiv.cdat,indiv.crand) -->

<!-- length.plot <- ggplot() + -->
<!--     geom_point(data = plot.dat, aes(x = perc, y = lengths,colour=group),size=2) -->

<!-- length.plot -->

<!-- ``` -->

Fscores for individual users during individual annotation filtering. Blue is iterative removal, red is random

```{r indiv-perf, echo=0,fig.height=10}

crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
crowd.perf$perc <- 0

indiv.dat <- crowd.perf
indiv.rand <- crowd.perf

for(i in 1:9){

    rejects <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rejections.tsv',i*10),stringsAsFactors=F,header=F)
    rejects$V1 <- sapply(rejects$V1,function(x) substr(x,1,nchar(x)-4))
    reject.slice <- subset(crowd.perf,class_id %in% rejects$V1)
    reject.slice$perc <- i
    indiv.dat <- rbind(indiv.dat,reject.slice)

    rand.rejects <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rand_rejections.tsv',i*10),stringsAsFactors=F,header=F)
    rand.rejects$V1 <- sapply(rand.rejects$V1,function(x) substr(x,1,nchar(x)-4))
    rand.slice <- subset(crowd.perf,class_id %in% rand.rejects$V1)
    rand.slice$perc <- i
    indiv.rand <- rbind(indiv.rand,rand.slice)
}


indiv.dat$precision <- apply(indiv.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.dat$recall <- apply(indiv.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.dat$fscore <- apply(indiv.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
indiv.dat$fscore[indiv.dat$tp==0] <- 0
indiv.dat$group <- 'IR'

indiv.rand$precision <- apply(indiv.rand[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.rand$recall <- apply(indiv.rand[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.rand$fscore <- apply(indiv.rand[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
indiv.rand$fscore[indiv.rand$tp==0] <- 0
indiv.rand$group <- 'Random'

plot.dat <- rbind(indiv.dat,indiv.rand) 

perf.plots <- vector('list',9)
rand.sub <- subset(plot.dat,group=='IR' & perc==0)
for(i in 0:8){

    sub.dat <- subset(plot.dat,perc==i)

    ir.sub <- subset(sub.dat,group=='IR')
    rand.sub <- subset(sub.dat,group=='Random')

    sub.plot <- ggplot() +
        ## geom_density(data=sub.dat,aes(x=fscore,group=group,colour=group,fill=group),alpha=0.4) +
        geom_histogram(data=ir.sub,aes(x=fscore,y=..count../sum(..count..),colour='blue',fill='blue',alpha=0.4),binwidth=0.1,alpha=0.4) +
        geom_histogram(data=rand.sub,aes(x=fscore,y=..count../sum(..count..),colour='red',fill='red',alpha=0.4),binwidth=0.1,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('Iter %d',i+1))

    perf.plots[[i+1]] <- sub.plot

}

perf.plot <- marrangeGrob(perf.plots,ncol=3,nrow=3)
perf.plot

```

```{r indiv-filtering-lengths, echo=0}

crowd.summ <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_summary.tsv',sep='\t',header=T)

length.dat <- crowd.summ
length.dat$perc <- 0

length.rand <- crowd.summ
length.rand$perc <- 0

for(i in 1:8){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rejections.tsv',i*10),stringsAsFactors=F)
    data <- sapply(data$V1,function(x) substr(x,1,nchar(x)-4))
    data <- subset(crowd.summ,(class_id %in% data))
    data$perc <- i
    length.dat <- rbind(length.dat,data)

    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rand_rejections.tsv',i*10),stringsAsFactors=F)
    data <- sapply(data$V1,function(x) substr(x,1,nchar(x)-4))    
    data <- subset(crowd.summ,(class_id %in% data))
    data$perc <- i
    length.rand <- rbind(length.rand,data)

}

## length.dat$precision <- apply(length.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$recall <- apply(length.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$fscore <- apply(length.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
## length.dat$fscore[length.dat$tp==0] <- 0
#length.dat$lengths <- apply(length.dat[,c("tp","fp")],1,function(x) x[1]+x[2])

length.plots <- vector('list',9)

for(i in 0:8){

    length.slice <- subset(length.dat,perc == i)
    rand.slice <- subset(length.rand,perc == i)
        
    sub.plot <- ggplot() +
        geom_histogram(data=length.slice,aes(x=anno_len,y=..count../sum(..count..)),colour='blue',fill='blue',binwidth=10,alpha=0.4) +
        geom_histogram(data=rand.slice,aes(x=anno_len,y=..count../sum(..count..)),colour='red',fill='red',binwidth=10,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('Iteration %d, %d',i,nrow(length.slice))) 

    
    length.plots[[i+1]] <- sub.plot
}

dotGrob <- marrangeGrob(length.plots,ncol=3,nrow=3)
dotGrob

```

Fscores for individual users during user group filtering

```{r user-perf-group, echo=0,fig.height=10}

crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
crowd.perf$perc <- 0

indiv.dat <- crowd.perf
indiv.rand <- crowd.perf

for(i in 1:9){

    rejects <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rejections.tsv',i*10),stringsAsFactors=F,header=F)
    rejects$V1 <- sapply(rejects$V1,function(x) substr(x,1,nchar(x)-4))
    reject.slice <- subset(crowd.perf,class_id %in% rejects$V1)
    reject.slice$perc <- i
    indiv.dat <- rbind(indiv.dat,reject.slice)

    rand.rejects <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rand_rejections.tsv',i*10),stringsAsFactors=F,header=F)
    rand.rejects$V1 <- sapply(rand.rejects$V1,function(x) substr(x,1,nchar(x)-4))
    rand.slice <- subset(crowd.perf,class_id %in% rand.rejects$V1)
    rand.slice$perc <- i
    indiv.rand <- rbind(indiv.rand,rand.slice)
}


indiv.dat$precision <- apply(indiv.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.dat$recall <- apply(indiv.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.dat$fscore <- apply(indiv.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
indiv.dat$fscore[indiv.dat$tp==0] <- 0
indiv.dat$group <- 'IR'

indiv.rand$precision <- apply(indiv.rand[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.rand$recall <- apply(indiv.rand[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
indiv.rand$fscore <- apply(indiv.rand[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
indiv.rand$fscore[indiv.rand$tp==0] <- 0
indiv.rand$group <- 'Random'

plot.dat <- rbind(indiv.dat,indiv.rand) 

perf.plots <- vector('list',9)

for(i in 0:8){

    sub.dat <- subset(plot.dat,perc==i)

    ir.sub <- subset(sub.dat,group=='IR')
    rand.sub <- subset(sub.dat,group=='Random')

    sub.plot <- ggplot() +
        ## geom_density(data=sub.dat,aes(x=fscore,group=group,colour=group,fill=group),alpha=0.4) +
        geom_histogram(data=ir.sub,aes(x=fscore,y=..count../sum(..count..),colour='blue',fill='blue',alpha=0.4),binwidth=0.1,alpha=0.4) +
        geom_histogram(data=rand.sub,aes(x=fscore,y=..count../sum(..count..),colour='red',fill='red',alpha=0.4),binwidth=0.1,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('Iter %d',i+1))

    perf.plots[[i+1]] <- sub.plot

}

perf.plot <- marrangeGrob(perf.plots,ncol=3,nrow=3)
perf.plot

```

```{r user-filtering-lengths, echo=0}

crowd.summ <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_summary.tsv',sep='\t',header=T)

length.dat <- crowd.summ
length.dat$perc <- 0

length.rand <- crowd.summ
length.rand$perc <- 0

for(i in 1:8){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rejections.tsv',i*10),stringsAsFactors=F)
    data <- sapply(data$V1,function(x) substr(x,1,nchar(x)-4))
    data <- subset(crowd.summ,(class_id %in% data))
    data$perc <- i
    length.dat <- rbind(length.dat,data)

    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rand_rejections.tsv',i*10),stringsAsFactors=F)
    data <- sapply(data$V1,function(x) substr(x,1,nchar(x)-4))    
    data <- subset(crowd.summ,(class_id %in% data))
    data$perc <- i
    length.rand <- rbind(length.rand,data)

}

## length.dat$precision <- apply(length.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$recall <- apply(length.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$fscore <- apply(length.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
## length.dat$fscore[length.dat$tp==0] <- 0
#length.dat$lengths <- apply(length.dat[,c("tp","fp")],1,function(x) x[1]+x[2])

length.plots <- vector('list',9)

for(i in 0:8){

    length.slice <- subset(length.dat,perc == i)
    rand.slice <- subset(length.rand,perc == i)
        
    sub.plot <- ggplot() +
        geom_histogram(data=length.slice,aes(x=anno_len,y=..count../sum(..count..)),colour='blue',fill='blue',binwidth=10,alpha=0.4) +
        geom_histogram(data=rand.slice,aes(x=anno_len,y=..count../sum(..count..)),colour='red',fill='red',binwidth=10,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('Iteration %d, %d',i,nrow(length.slice)))
    
    length.plots[[i+1]] <- sub.plot
}

dotGrob <- marrangeGrob(length.plots,ncol=3,nrow=3)
dotGrob

```


[Location of user test](http://sulab.scripps.edu/jbrugg/reports/3-23-16-meeting.html)

```{r time-hist, echo=0}

times <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv',sep='\t',header=T)

times$formatted.times <- strptime(times$time,"%H:%M:%S")

time.hist <- ggplot() +
    geom_histogram(data=times,aes(x=formatted.times),binwidth=1) +
    xlab('Minutes') +
    ylab('') +
    ggtitle('Time per classification') +
    theme(plot.title = element_text(size=20))

time.hist 

```

```{r select-time-hist, echo=0}

times <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv',sep='\t',header=T)

times$formatted.times <- strptime(times$time,"%H:%M:%S")
times <- subset(times,formatted.times < strptime('0:10:00',"%H:%M:%S"))

time.hist <- ggplot() + geom_histogram(data=times,aes(x=formatted.times),binwidth=1) +
    xlab('Minutes') +
    ylab('') +
    ggtitle('Time per classification') +
    theme(plot.title = element_text(size=20))

time.hist 

```

```{r group-size, echo=0}

time.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/user_times.tsv',sep='\t',header=T)

session.hist <- ggplot() +
    geom_histogram(data=time.data,aes(x=num_class),binwidth=1) +
    ggtitle(sprintf('Average session length = %f images',mean(time.data$num_class)))

session.hist

```

Users' fscores taken in various interations: black is first, blue is second, red is third, green is fourth, and purple is fifth.

```{r users-rejected00, echo=0}

user.dat <- data.frame()

for(i in 1:9){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/user_rejections.tsv',i*10))
    data$perc <- i
    user.dat <- rbind(user.dat,data)
}

crowd.summ <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_summary.tsv',sep='\t',header=T)
crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
crowd.perf$precision <- apply(crowd.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$recall <- apply(crowd.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$fscore <- apply(crowd.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
crowd.perf$fscore[crowd.perf$tp==0] <- 0

first.users <- subset(user.dat,perc==1 | perc==2 | perc==3 | perc==4 | perc==5)
#first.users <- subset(user.dat,perc==1)
crowd.slice <- subset(crowd.perf,user_id %in% first.users$V1)

# time taken for rejected users, anno lengths, quality marks, amount of work done.

users <- unique(crowd.slice$user_id)

user.plots <- vector('list',length(users)) ## + length(user.freq) %/% 10 + 1) 
cnt <- 0

colors <- c('black','blue','red','green','purple')

for(i in users){
    color.cnt <- subset(user.dat,V1==i)$perc
    cnt <- cnt+1

    user.frame <- subset(crowd.slice,user_id==i)
    user.frame$x <- 0
    user.plot <- ggplot() +
        geom_point(data=user.frame,aes_string(x='x',y='fscore'),colour=colors[color.cnt]) +
        xlab('') +
        ylab('') +
        ylim(0,1.0) +
        theme(axis.text.x = element_blank(),axis.ticks.x = element_blank()) +
        coord_fixed(ratio=5)

    if(cnt%%10 != 1){
        user.plot <- user.plot + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank())
    }
    ## if(cnt%%10 == 9){
    ##     user.plot <- user.plot + theme(
    ## }
    
    user.plots[[cnt]] <- user.plot
}

dotGrob <- marrangeGrob(user.plots,ncol=10,nrow=4,widths=c(1.27,rep(1,9)))
dotGrob

```

Length of rejected users' annotations, same colors

```{r rejected-lengths, echo=0, warning=0}

crowd.slice <- subset(crowd.summ,user_id %in% first.users$V1)

# time taken for rejected users, anno lengths, quality marks, amount of work done.

#users <- unique(crowd.slice$user_id)

users <- sort(table(crowd.slice$user_id),TRUE)

                                        #user.plots <- vector('list',length(users)) ## + length(user.freq) %/% 10 + 1)
user.plots <- vector('list',80)
cnt <- 0

colors <- c('black','blue','red','green','purple')

for(i in names(users[1:80])){
    color.cnt <- subset(user.dat,V1==i)$perc
    cnt <- cnt+1

    user.frame <- subset(crowd.slice,user_id==i)
    user.frame$x <- 0
    user.plot <- ggplot() +
        geom_point(data=user.frame,aes_string(x='x',y='anno_len'),colour=colors[color.cnt]) +
        xlab('') +
        ylab('') +
        theme(axis.text.x = element_blank(),axis.ticks.x = element_blank(),axis.text.y = element_text(size=4)) 


    ## if(cnt%%10 != 1){
    ##     user.plot <- user.plot + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank())
    ## }
    ## if(cnt%%10 == 9){
    ##     user.plot <- user.plot + theme(
    ## }
    
    user.plots[[cnt]] <- user.plot
}

dotGrob <- marrangeGrob(user.plots,ncol=10,nrow=4,widths=c(1.27,rep(1.27,9)))
dotGrob

```

Individual performance during time filtering

```{r time-filtering-perf, echo=0}

## crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T,stringsAsFactors=F)
## crowd.perf$time <- 0

## time.dat <- crowd.perf

## for(i in 1:8){

##     rejects <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/rejections.tsv',i*10),stringsAsFactors=F,header=F)
##     reject.slice <- subset(crowd.perf,class_id %in% rejects$V1)
##     reject.slice$time <- rep(i*30,nrow(reject.slice))
##     time.dat <- rbind(time.dat,reject.slice)

## }


## time.dat$precision <- apply(time.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
## time.dat$recall <- apply(time.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
## time.dat$fscore <- apply(time.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
## time.dat$fscore[time.dat$tp==0] <- 0



time.dat <- data.frame()

for(i in 0:8){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/indiv_gold.tsv',i*10),header=T)
    data$time <- i*30
    time.dat <- rbind(time.dat,data)
}

time.dat$precision <- apply(time.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
time.dat$recall <- apply(time.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
time.dat$fscore <- apply(time.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
time.dat$fscore[time.dat$tp==0] <- 0

time.plots <- vector('list',9)

for(i in 0:8){

    time.slice <- subset(time.dat,time == i*30)
    sub.plot <- ggplot() +
        geom_histogram(data=time.slice,aes(x=fscore,y=..count../sum(..count..)),colour='blue',fill='blue',binwidth=0.1,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('%d Seconds, %d',i*10,nrow(time.slice)))
    
    ## if(i%%10 != 1){
    ##     sub.plot <- sub.plot + theme(axis.text.y = element_blank(), axis.ticks.y = element_blank())
    ## }
    
    time.plots[[i+1]] <- sub.plot
}

## dotGrob <- marrangeGrob(time.plots,ncol=10,nrow=4,widths=c(1.27,rep(1,9)))
dotGrob <- marrangeGrob(time.plots,ncol=3,nrow=3)
dotGrob

```

Length of annotations during time filtering

```{r time-filtering-lengths, echo=0}

crowd.summ <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_summary.tsv',sep='\t',header=T)

length.dat <- crowd.summ
length.dat$time <- 0

for(i in 1:8){
    data <- read.table(sprintf('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/rejections.tsv',i*10))
    data <- subset(crowd.summ,!(class_id %in% data$V1))
    data$time <- i*30
    length.dat <- rbind(length.dat,data)
}

## length.dat$precision <- apply(length.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$recall <- apply(length.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
## length.dat$fscore <- apply(length.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
## length.dat$fscore[length.dat$tp==0] <- 0
#length.dat$lengths <- apply(length.dat[,c("tp","fp")],1,function(x) x[1]+x[2])

length.plots <- vector('list',9)

for(i in 0:8){

    length.slice <- subset(length.dat,time == i*30)
    sub.plot <- ggplot() +
        geom_histogram(data=length.slice,aes(x=anno_len,y=..count../sum(..count..)),colour='blue',fill='blue',binwidth=2,alpha=0.4) +
        xlab('') +
        ylab('') +
        guides(fill=FALSE,colour=FALSE,group=FALSE) +
        ggtitle(sprintf('%d Seconds, %d',i*10,nrow(length.slice)))
    
    length.plots[[i+1]] <- sub.plot
}

dotGrob <- marrangeGrob(length.plots,ncol=3,nrow=3)
dotGrob

```

Annotation time vs performance 

```{r time-vs-perf, echo=0}

time2int <- function(x){
    
    splitx <- strsplit(x,':')[[1]]
    seconds <- 3600*as.numeric(splitx[1]) + 60*as.numeric(splitx[2]) + as.numeric(splitx[3])
    seconds

}
crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
time.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv',sep='\t',header=T,stringsAsFactors=F)

crowd.perf <- subset(crowd.perf,class_id %in% time.perf$class_id)
time.perf <- subset(time.perf,class_id %in% crowd.perf$class_id)

crowd.perf <- crowd.perf[order(crowd.perf$class_id),]
time.perf <- time.perf[order(time.perf$class_id),]

crowd.perf$precision <- apply(crowd.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$recall <- apply(crowd.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$fscore <- apply(crowd.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
crowd.perf$fscore[crowd.perf$tp==0] <- 0

time.perf$fmttime <- sapply(time.perf$time,time2int)

time2perf <- cbind.data.frame(crowd.perf[,c("quality","subj_id","fscore","anno_length")],time=time.perf$fmttime)

time.plot <- ggplot() +
    geom_jitter(data=time2perf,aes(x=time,y=fscore),alpha=0.4,colour='green') +
    scale_x_continuous(breaks=seq(0,max(time2perf$time),60)) 

time.plot

```


```{r time-vs-length, echo=0}

time2int <- function(x){
    
    splitx <- strsplit(x,':')[[1]]
    seconds <- 3600*as.numeric(splitx[1]) + 60*as.numeric(splitx[2]) + as.numeric(splitx[3])
    seconds

}

crowd.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
time.perf <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv',sep='\t',header=T,stringsAsFactors=F)

crowd.perf <- subset(crowd.perf,class_id %in% time.perf$class_id)
time.perf <- subset(time.perf,class_id %in% crowd.perf$class_id)

crowd.perf <- crowd.perf[order(crowd.perf$class_id),]
time.perf <- time.perf[order(time.perf$class_id),]

crowd.perf$precision <- apply(crowd.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$recall <- apply(crowd.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
crowd.perf$fscore <- apply(crowd.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
crowd.perf$fscore[crowd.perf$tp==0] <- 0

time.perf$fmttime <- sapply(time.perf$time,time2int)

time2perf <- cbind.data.frame(crowd.perf[,c("class_id","quality","subj_id","fscore","anno_length")],time=time.perf$fmttime)

time.plot <- ggplot() +
    geom_point(data=time2perf,aes(x=time,y=anno_length),alpha=0.4,colour='green') +
    scale_x_continuous(breaks=seq(0,max(time2perf$time),60))

time.plot

```
