library(ggplot2)
library(reshape2)
library(gridExtra)

######################
## c - ROC curve
######################

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
anno.cnts <- table(crowd.data$subj_id)
cnt.df <- data.frame(anno.cnts)
cnt.df <- cnt.df[order(cnt.df$Var1),]
cnt.df <- subset(cnt.df, Freq > 4)

voted_perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/voted_performance.tsv",sep = "\t",header = T)
voted_perf <- subset(voted_perf,subj_id %in% cnt.df$Var1)

turk.perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
mturk.perf <- melt(turk.perf,id=c("subj_id","v"))
turk.perf <- dcast(mturk.perf,subj_id+v~variable,mean)

m.zoo <- melt(voted_perf,id=c('v'))
c.zoo <- dcast(m.zoo, v~variable, sum)

m.turk <- melt(turk.perf,id=c('v'))
c.turk <- dcast(m.turk, v~variable,sum)

c.zoo$precision <- apply(c.zoo[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
c.zoo$recall <- apply(c.zoo[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
c.zoo$fscore <- apply(c.zoo[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
c.zoo$group <- "Zooniverse"

c.turk$precision <- apply(c.turk[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
c.turk$recall <- apply(c.turk[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
c.turk$precision[is.nan(c.turk$precision)] <- 1
c.turk$recall[is.nan(c.turk$recall)] <- 1
c.turk$fscore <- apply(c.turk[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
c.turk$fscore[is.nan(c.turk$fscore)] <- 0
c.turk$group <- "AMT"

plot.dat <- rbind(c.zoo[,c("v",'precision','recall','group')],c.turk[,c("v",'precision','recall','group')])
roc.plot <- ggplot(data=plot.dat, aes(x= 1-precision, y= recall,colour=group)) +
    geom_point(size=3.5) +
    geom_line(aes(group=group)) +
    xlim(0,1.0) +
    ylim(0,1.0) +
    theme(plot.title = element_text(size=20)) +
    ylab('Recall') +
    xlab('1 - Precision') +
    scale_colour_discrete(name='')

ggsave('figs/c_roc.png')

####################

####################
## i - Fscore voted
####################

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
anno.cnts <- table(crowd.data$subj_id)
cnt.df <- data.frame(anno.cnts)
cnt.df <- cnt.df[order(cnt.df$Var1),]
cnt.df <- subset(cnt.df, Freq > 4)

voted_perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
voted_perf <- subset(voted_perf,subj_id %in% cnt.df$Var1) # remove incomplete subjects

turk.perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
mturk.perf <- melt(turk.perf,id=c("subj_id","v"))
turk.perf <- dcast(mturk.perf,subj_id+v~variable,mean)
turk.perf <- subset(turk.perf,v==0)

m.zoo <- melt(voted_perf,id=c("v","subj_id"))
avg.zoo <- dcast(m.zoo,v+subj_id~variable,mean)
avg.zoo <- subset(avg.zoo,v==0)

avg.zoo$precision <- apply(avg.zoo[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$recall <- apply(avg.zoo[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$fscore <- apply(avg.zoo[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
avg.zoo$group <- "Zooniverse"
## c.zoo <- subset(c.zoo,v<7)

turk.perf$precision <- apply(turk.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$recall <- apply(turk.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$precision[is.nan(turk.perf$precision)] <- 1
turk.perf$recall[is.nan(turk.perf$recall)] <- 1
turk.perf$fscore <- apply(turk.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.perf$fscore[is.nan(turk.perf$fscore)] <- 0
turk.perf$group <- "AMT"

voted.hist <- ggplot() +
    geom_histogram(data=avg.zoo,aes(x=fscore,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.perf,aes(x=fscore,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Fscore after Voting") +
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) +
    xlab('Fscore')
 
ggsave('figs/i_voted_fscore.png')

####################

####################
## h - Recall voted
####################

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
anno.cnts <- table(crowd.data$subj_id)
cnt.df <- data.frame(anno.cnts)
cnt.df <- cnt.df[order(cnt.df$Var1),]
cnt.df <- subset(cnt.df, Freq > 4)

voted_perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
voted_perf <- subset(voted_perf,subj_id %in% cnt.df$Var1) # remove incomplete subjects

turk.perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
mturk.perf <- melt(turk.perf,id=c("subj_id","v"))
turk.perf <- dcast(mturk.perf,subj_id+v~variable,mean)
turk.perf <- subset(turk.perf,v==0)

m.zoo <- melt(voted_perf,id=c("v","subj_id"))
avg.zoo <- dcast(m.zoo,v+subj_id~variable,mean)
avg.zoo <- subset(avg.zoo,v==0)

avg.zoo$precision <- apply(avg.zoo[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$recall <- apply(avg.zoo[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$fscore <- apply(avg.zoo[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
avg.zoo$group <- "Zooniverse"
## c.zoo <- subset(c.zoo,v<7)

turk.perf$precision <- apply(turk.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$recall <- apply(turk.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$precision[is.nan(turk.perf$precision)] <- 1
turk.perf$recall[is.nan(turk.perf$recall)] <- 1
turk.perf$fscore <- apply(turk.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.perf$fscore[is.nan(turk.perf$fscore)] <- 0
turk.perf$group <- "AMT"

voted.hist <- ggplot() +
    geom_histogram(data=avg.zoo,aes(x=recall,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.perf,aes(x=recall,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Recall after Voting") +
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) +
    xlab('Recall')
 
ggsave('figs/h_voted_recall.png')

####################

####################
## g - Precision voted
####################

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',sep='\t',header=T)
anno.cnts <- table(crowd.data$subj_id)
cnt.df <- data.frame(anno.cnts)
cnt.df <- cnt.df[order(cnt.df$Var1),]
cnt.df <- subset(cnt.df, Freq > 4)

voted_perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
voted_perf <- subset(voted_perf,subj_id %in% cnt.df$Var1) # remove incomplete subjects

turk.perf <- read.table("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv",sep = "\t",header = T)
mturk.perf <- melt(turk.perf,id=c("subj_id","v"))
turk.perf <- dcast(mturk.perf,subj_id+v~variable,mean)
turk.perf <- subset(turk.perf,v==0)

m.zoo <- melt(voted_perf,id=c("v","subj_id"))
avg.zoo <- dcast(m.zoo,v+subj_id~variable,mean)
avg.zoo <- subset(avg.zoo,v==0)

avg.zoo$precision <- apply(avg.zoo[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$recall <- apply(avg.zoo[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
avg.zoo$fscore <- apply(avg.zoo[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
avg.zoo$group <- "Zooniverse"
## c.zoo <- subset(c.zoo,v<7)

turk.perf$precision <- apply(turk.perf[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$recall <- apply(turk.perf[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.perf$precision[is.nan(turk.perf$precision)] <- 1
turk.perf$recall[is.nan(turk.perf$recall)] <- 1
turk.perf$fscore <- apply(turk.perf[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.perf$fscore[is.nan(turk.perf$fscore)] <- 0
turk.perf$group <- "AMT"

voted.hist <- ggplot() +
    geom_histogram(data=avg.zoo,aes(x=precision,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.perf,aes(x=precision,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Precision after Voting") +
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) +
    xlab('Precision')
 
ggsave('figs/g_voted_precision.png')

####################

####################
## f - fscore indiv
####################

zoo.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',header=T,sep='\t')

zoo.dat$precision <- apply(zoo.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall <- apply(zoo.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall[is.nan(zoo.dat$recall)] <- 1
zoo.dat$precision[is.nan(zoo.dat$precision)] <- 1
zoo.dat$fscore <- apply(zoo.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.dat$fscore[is.nan(zoo.dat$fscore)] <- 0

turk.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/crowd_performance.tsv',header=T,sep='\t')
turk.dat$precision <- apply(turk.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall <- apply(turk.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall[is.nan(turk.dat$recall)] <- 1
turk.dat$precision[is.nan(turk.dat$precision)] <- 1
turk.dat$fscore <- apply(turk.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.dat$fscore[is.nan(turk.dat$fscore)] <- 0

indiv.hist <- ggplot() +
    geom_histogram(data=zoo.dat,aes(x=fscore,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.dat,aes(x=fscore,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Individual Fscores") +
    xlab('Fscore') + 
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) 

 
ggsave('figs/f-fscore-indiv.png')

####################

####################
## e - recall indiv
####################

zoo.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',header=T,sep='\t')

zoo.dat$precision <- apply(zoo.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall <- apply(zoo.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall[is.nan(zoo.dat$recall)] <- 1
zoo.dat$precision[is.nan(zoo.dat$precision)] <- 1
zoo.dat$fscore <- apply(zoo.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.dat$fscore[is.nan(zoo.dat$fscore)] <- 0

turk.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/crowd_performance.tsv',header=T,sep='\t')
turk.dat$precision <- apply(turk.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall <- apply(turk.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall[is.nan(turk.dat$recall)] <- 1
turk.dat$precision[is.nan(turk.dat$precision)] <- 1
turk.dat$fscore <- apply(turk.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.dat$fscore[is.nan(turk.dat$fscore)] <- 0

indiv.hist <- ggplot() +
    geom_histogram(data=zoo.dat,aes(x=recall,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.dat,aes(x=recall,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Individual Recall") +
    xlab('Recall') + 
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) 

 
ggsave('figs/e-recall-indiv.png')

####################

####################
## d - recall indiv
####################

zoo.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',header=T,sep='\t')

zoo.dat$precision <- apply(zoo.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall <- apply(zoo.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall[is.nan(zoo.dat$recall)] <- 1
zoo.dat$precision[is.nan(zoo.dat$precision)] <- 1
zoo.dat$fscore <- apply(zoo.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.dat$fscore[is.nan(zoo.dat$fscore)] <- 0

turk.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/crowd_performance.tsv',header=T,sep='\t')
turk.dat$precision <- apply(turk.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall <- apply(turk.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall[is.nan(turk.dat$recall)] <- 1
turk.dat$precision[is.nan(turk.dat$precision)] <- 1
turk.dat$fscore <- apply(turk.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.dat$fscore[is.nan(turk.dat$fscore)] <- 0

indiv.hist <- ggplot() +
    geom_histogram(data=zoo.dat,aes(x=precision,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.dat,aes(x=precision,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Individual Precision") +
    xlab('Precision') + 
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) 

ggsave('figs/d-precision-indiv.png')

####################

####################
## j - # particles per indiv
####################

zoo.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',header=T,sep='\t')

zoo.dat$precision <- apply(zoo.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall <- apply(zoo.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall[is.nan(zoo.dat$recall)] <- 1
zoo.dat$precision[is.nan(zoo.dat$precision)] <- 1
zoo.dat$fscore <- apply(zoo.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.dat$fscore[is.nan(zoo.dat$fscore)] <- 0

turk.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/crowd_performance.tsv',header=T,sep='\t')
turk.dat$precision <- apply(turk.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall <- apply(turk.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall[is.nan(turk.dat$recall)] <- 1
turk.dat$precision[is.nan(turk.dat$precision)] <- 1
turk.dat$fscore <- apply(turk.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.dat$fscore[is.nan(turk.dat$fscore)] <- 0

indiv.hist <- ggplot() +
    geom_histogram(data=zoo.dat,aes(x=anno_length,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,stat='bin') +
    geom_histogram(data=turk.dat,aes(x=anno_length,y=..count../sum(..count..),fill='red',colour='red'),alpha=0.5,stat='bin') + 
    ggtitle("Number of particles per Annotation") +
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Zoo", "AMT")) +
    theme(axis.title.y = element_blank(),
          plot.title=element_text(size=18,hjust=0.5)) +
    xlab('Number of Particles per Annotation')
 
ggsave('figs/j-anno-length.png')

####################

####################
## k - # particles vs recall 
####################

zoo.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/crowd_performance.tsv',header=T,sep='\t')
zoo.dat$precision <- apply(zoo.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall <- apply(zoo.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
zoo.dat$recall[is.nan(zoo.dat$recall)] <- 1
zoo.dat$precision[is.nan(zoo.dat$precision)] <- 1
zoo.dat$fscore <- apply(zoo.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
zoo.dat$fscore[is.na(zoo.dat$fscore)] <- 0

turk.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/crowd_performance.tsv',header=T,sep='\t')
turk.dat$precision <- apply(turk.dat[,c("tp","fp")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall <- apply(turk.dat[,c("tp","fn")], 1, function(x) x[1]/(x[2]+x[1]))
turk.dat$recall[is.nan(turk.dat$recall)] <- 1
turk.dat$precision[is.nan(turk.dat$precision)] <- 1
turk.dat$fscore <- apply(turk.dat[,c("precision","recall")], 1, function(x) 2*x[1]*x[2]/(x[1]+x[2]))
turk.dat$fscore[is.nan(turk.dat$fscore)] <- 0

zoo.dat <- zoo.dat[,c("precision","recall","fscore","anno_length")]
zoo.cor <- round(cor(zoo.dat),2)
zoo.cor[upper.tri(zoo.cor,diag=FALSE)] <- NA
melt.zoo.cor <- melt(zoo.cor, na.rm = TRUE)


turk.dat <- turk.dat[,c('precision','recall','fscore','anno_length')]
turk.cor <- round(cor(turk.dat),2)
turk.cor[lower.tri(turk.cor,diag=TRUE)] <- NA
melt.turk.cor <- melt(turk.cor, na.rm = TRUE)

cor.mat <- rbind(melt.zoo.cor,melt.turk.cor)

cor.heat <- ggplot(data=cor.mat,aes(Var2,Var1,fill=value)) +
    geom_tile(color='white') +
    geom_text(aes(Var2,Var1,label=value),color='black',size=4) +
    scale_fill_gradient2(low='blue',high='red',mid='white',midpoint=0,limit=c(-1,1),space="Lab", name='Correlation') +
    theme_minimal() +
    theme(axis.text.x = element_text(angle=45,vjust=1,size=12,hjust=1),
          axis.text.y = element_text(size=12,angle=45),
          axis.title.x = element_text(size=16,hjust=1,vjust=3.9,angle=45),
          axis.title.y = element_text(size=16,vjust=1.05,hjust=0.95,angle=45)) +
    scale_x_discrete(position='bottom',name='AMT') +
    scale_y_discrete(position='left',name='Zooniverse') + 
    coord_fixed()

ggsave('figs/k-cor-heat.png')

####################

####################
## l - inter-intra expert agreement
####################

intra.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/intra_agreement.tsv',header=T,sep='\t')

intra.data$agreement <- apply(intra.data[,c("intersection","union")],1,function(x) x[1]/x[2])
intra.data[intra.data$union==0,]$agreement <- 1

inter.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/inter_agreement.tsv',header=T,sep='\t')

inter.data$agreement <- apply(inter.data[,c("intersection","union")],1,function(x) x[1]/x[2])
inter.data[inter.data$union==0,]$agreement <- 1

inter.agreement.hist <- ggplot() +
    geom_histogram(data=inter.data,aes(x=agreement,y=..count../sum(..count..),colour='red',fill='red'),alpha=0.5,na.rm=T) +
    geom_histogram(data=intra.data,aes(x=agreement,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5,na.rm=T) +
    geom_hline(yintercept=0,colour='white',size=0.65) +
    ggtitle('Expert Agreement') +
    scale_colour_manual(name="", values=c("red", "blue"), labels=c("Intra", "Inter")) + 
    scale_fill_manual(name="", values=c("red", "blue"), labels=c("Intra", "Inter")) +
    scale_alpha_manual(name='',values=c("red", "blue"), labels=c("Intra", "Inter")) +
    ## scale_x_continuous(limits=c(0,1)) + 
    ylim(0,.35) +
    theme(axis.title.y = element_blank(),plot.title = element_text(size=18,hjust=0.5)) +
    xlab('Agreement') 


ggsave('figs/l-expert-agreement.png')

####################

####################
## m - inter-user vs inter-expert agreement
####################

crowd.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/inter_user_agreement.tsv',sep='\t',header=T)

crowd.data$agreement <- apply(crowd.data[,c('intersection','union')],1,function(x) x[2]/x[1])
crowd.data[crowd.data$union==0,]$agreement <- 1

expert.data <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/inter_agreement.tsv',header=T,sep='\t')

expert.data$agreement <- apply(expert.data[,c("intersection","union")],1,function(x) x[1]/x[2])
expert.data[expert.data$union==0,]$agreement <- 1

inter.agreement.hist <- ggplot() +
    geom_histogram(data=crowd.data,aes(x=agreement,y=..count../sum(..count..),colour='red',fill='red'),alpha=0.5) +
    geom_histogram(data=expert.data,aes(x=agreement,y=..count../sum(..count..),colour='blue',fill='blue'),alpha=0.5) + 
    ggtitle('') +
    scale_colour_manual(name="", values=c("blue", "red"), labels=c("Crowd", "Expert")) +
    scale_fill_manual(name="", values=c("blue", "red"), labels=c("Crowd", "Expert")) +
    scale_alpha_manual(name='',values=c("blue", "red"), labels=c("Crowd", "Expert")) +
    ## xlim(0,1.1) +
    ## ylim(0,.35) +
    theme(axis.title.y = element_blank(),plot.title = element_text(size=18,hjust=0.5)) +
    xlab('Agreement') 

ggsave('figs/m-expert-vs-crowd-agreement.png')

####################

####################
## q - Zoo FSC
####################

fsc.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/relion/results/zoo_fsc.fsc',sep='\t')

res.point <- fsc.dat[order(abs(fsc.dat$V2-.143)),][1,'V1']

fsc.plot <- ggplot() +
    geom_line(data=fsc.dat,aes(x=V1,y=V2)) +
    geom_hline(yintercept=.143,colour='red') +
    geom_vline(xintercept=res.point,colour='blue') +
    labs(title=sprintf('Crowd Resolution = %.3f Å',1/res.point),x='Resolution (1/Å)',y='FSC')

ggsave('figs/q-zoo-fsc.png')

####################
## r - Templ FSC
####################

fsc.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/relion/results/templ_fsc.fsc',sep='\t')

res.point <- fsc.dat[order(abs(fsc.dat$V2-.143)),][1,'V1']

fsc.plot <- ggplot() +
    geom_line(data=fsc.dat,aes(x=V1,y=V2)) +
    geom_hline(yintercept=.143,colour='red') +
    geom_vline(xintercept=res.point,colour='blue') +
    labs(title=sprintf('Crowd Resolution = %.3f Å',1/res.point),x='Resolution (1/Å)',y='FSC')

ggsave('figs/r-templ-fsc.png')

####################
## s - Norm FSC
####################


fsc.dat <- read.table('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/relion/results/norm_templ_fsc.fsc',sep='\t')

res.point <- fsc.dat[order(abs(fsc.dat$V2-.143)),][1,'V1']

fsc.plot <- ggplot() +
    geom_line(data=fsc.dat,aes(x=V1,y=V2)) +
    geom_hline(yintercept=.143,colour='red') +
    geom_vline(xintercept=res.point,colour='blue') +
    labs(title=sprintf('Normalized Template Resolution = %.3f Å',1/res.point),x='Resolution (1/Å)',y='FSC')

ggsave('figs/s-norm-fsc.png')

