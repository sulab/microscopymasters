#!/usr/bin/python

import json

with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/data/zoo_conv.json','r') as in_file:
    turkGold_conv = json.load(in_file)

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json','r') as in_file:
    zooGold_conv = json.load(in_file)


rev_zoogold = {}

for key, val in zooGold_conv.iteritems():
    rev_zoogold[val] = key

turk2zoo = {}

print len(zooGold_conv)
print len(rev_zoogold)
print len(turkGold_conv)

for key, val in turkGold_conv.iteritems():
    #print rev_zoogold.keys()
    turk2zoo[key] = rev_zoogold[val]

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/turk2zoo_conv.json','w') as out_file:
    json.dump(turk2zoo,out_file)
