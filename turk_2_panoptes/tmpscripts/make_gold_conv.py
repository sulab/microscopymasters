#!/usr/bin/python

import os
import json

import requests
from rauth import OAuth2Service

my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

panoptes = OAuth2Service(
    client_id=my_id,
    client_secret=my_secret,
    name='panoptes',
    authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
    access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
    base_url="https://panoptes.zooniverse.org/"
)

params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}

r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

token = r.json()["access_token"]

header_get = {'Accept': 'application/vnd.api+json; version=1',
              'Content-Type': 'application/json',
              'Authorization' : 'Bearer %s' % token}

# gold_data = {}

# for subj_id in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/'):

#     subj_req = requests.get("https://panoptes.zooniverse.org/api/subjects/%s" % subj_id,headers=header_get)
#     img_key = subj_req.json()['subjects'][0]['metadata']['#std_file']

#     gold_data[img_key] = subj_id

# with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/tmp/gold_img.json','w') as out_file:
#     json.dump(gold_data,out_file)
    
# print len(gold_data)

with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/tmp/gold_img.json','r') as in_file:
    gold_data = json.load(in_file)


conv_dict = {}

for subj_id in os.listdir('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/user_results/annotations/'): 

    subj_req = requests.get("https://panoptes.zooniverse.org/api/subjects/%s" % subj_id,headers=header_get)
    img_key = subj_req.json()['subjects'][0]['metadata']['#std_file']
    
    try:
        conv_dict[subj_id] = gold_data[img_key]
    except KeyError:
        pass

    
with open('/Users/Jake/Documents/Projects/crowdProj/turk_2_panoptes/data/zoo_conv.json','w') as out_file:
    json.dump(conv_dict,out_file)


