library(plyr)
library(reshape2)
library(ggplot2)

data <- read.table("/Users/Jake/Documents/Projects/markTheCrowd/turk_2_panoptes/analyzed_results/performance.tsv",sep='\t',header=T,row.names=NULL)

data$agreenum <- apply(data[,c("total","agreement")],1,function(x) x[1]*x[2])

mdata <- melt(data,id<-c("d","v"))


cdata <- dcast(mdata,d+v ~ variable,sum)


cdata$precision <-apply(cdata[,c("tp","fp","fn")],1,function(x) x[1]/(x[1]+x[2]))
cdata$recall <- apply(cdata[,c("tp","fp","fn")],1,function(x) x[1]/(x[1]+x[3]))
cdata$fscore <- apply(cdata[,c("precision","recall")],1,function(x) 2*x[1]*x[2]/(x[1]+x[2]))

max_row <- cdata[cdata$fscore==max(cdata$fscore),]
max_summary <- data[data$d==max_row$d & data$v == max_row$v,]

cdata$d <- cdata$d*5
#cdata$agreement <- cagreenum / total

user_data <- read.table("analyzed_results/acceptions.tsv",sep='\t')

user_hist <- ggplot(user_data,aes(x=V5)) + geom_histogram()

perfect_users <- user_data[user_data$V5 == 1.0,]

roc_plot <- ggplot(cdata,aes(x=1-precision,y=recall))  + ggtitle(sprintf("Max F = %f",max_row$fscore))
roc_plot <- roc_plot + theme(axis.title=element_text(size=24), axis.text=element_text(size=24), plot.title=element_text(size=32)) + xlim(0,1.0) + ylim(0,1.0) +  geom_point(aes(colour=fscore),size=6)

gold_ids <- unique(user_data$V6)

gold_performance <- lapply(gold_ids,function(x) user_data[user_data$V6==x,]$V5)

gold_var <- sapply(gold_performance,var)
gold_nums <- sapply(gold_performance,length)

var_data <- data.frame(Variance=gold_var,Ids=factor(gold_ids, levels=gold_ids[order(gold_var)]),num=gold_nums)
#var_data <- var_data[order(var_data$Variance),]

#var_hist <- ggplot(var_data,aes(y=Variance,x=Ids)) + geom_point()
var_hist <- ggplot(var_data,aes(x=Ids,y=Variance)) + geom_bar(stat="identity")

