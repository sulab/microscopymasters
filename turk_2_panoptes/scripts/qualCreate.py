#!/home/jbrugg/env/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
import sys

ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
SECRET_KEY = '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'
HOST = 'mechanicalturk.amazonaws.com'

mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                      aws_secret_access_key=SECRET_KEY,
                      host=HOST)

hits = mtc.get_all_hits()

'''for hit in hits:
    if hit.expired:
        mtc.dispose_hit(hit.HITId)
        print "Closed HIT: " + str(hit.HITId)'''

instructions = Overview()
#instructions.append(FormattedContent('<font size="4">We are a group of scientists interested in studying the structure of proteins.</font>'))
#instructions.append(FormattedContent('<font size="4"> As part of our research, we need to pick protein particles out of high-noise microscopy images.</font>'))
#instructions.append(FormattedContent('<font size="4"> You will be helping us speed this picking process along. </font>'))
#instructions.append(FormattedContent('<font size="4"> Upon finishing this qualification, we will automatically generate a user account on Zooniverse for you to mark images on our platform. </font>'))
#instructions.append(FormattedContent('<font size="4"> This account is only for storing your results, and no personal information will be stored. </font>'))

instructions.append_field('Text','We are a group of scientists interested in studying the structure of proteins. As part of our research, we need to pick protein particles out of high-noise microscopy images. You will be helping us speed this picking process along. Upon finishing this qualification, we will automatically generate a user account on Zooniverse for you to mark images on our platform. This account is only for storing your results, and none of your personal information will be stored.')

'''instructions.append_field(FormattedContent('<font size="4">Here are some guidelines for perfect protein picking:\n </font>'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<p><font size="4">This is a microscopy image. </font></p>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/WtmaqVg.png" alt="Raw microscopy image" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Here is the same image with all of the proteins marked in red </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/YYGZqt6.png" alt="Marked Proteins" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">In the corner of many of the images there is a darker region. Particles in this region are not valid for study, so do not mark any here. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/1sMhxrk.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Some images contain dark aberrations. These are not proteins, so do not mark them. \n  </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/CWruYrG.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Sometimes proteins stick together. If you cannot clearly distinguish individual proteins do not mark them. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/sUAEyqf.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Here a mass of proteins are stuck together. Do not worry about marking these regions, as the proteins are too difficult to distinguish. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/iMPjCbh.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent(''))
instructions.append(FormattedContent('<hr></hr>'))

instructions.append(FormattedContent('<font size="6">Invalid Images</font>'))
instructions.append(FormattedContent('<font size="4">Some images have problems. Do not mark any proteins in these images. This image has a crack down the center. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/WydMOFI.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Some images have dark splotches on them. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/lj47Snf.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">Here is a more extreme example of splotching. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/hgwfYJd.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append(FormattedContent('<font size="4">In this image the sample moved during image collection, creating the smudges you see here. </font>'))
instructions.append(FormattedContent('<img src="http://i.imgur.com/aN4nI6z.jpg" alt="Carbon Ring" height="600" width="600" />'))
'''

instructions.append_field('Title','Here are some guidelines for perfect protein picking:')
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','This is a microscopy image.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/WtmaqVg.png" alt="Raw microscopy image" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','Here is the same image with all of the proteins marked in red.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/YYGZqt6.png" alt="Marked Proteins" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','In the corner of many of the images there is a darker region. Particles in and close to this region are too blurry, so do not mark any here.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/1sMhxrk.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','Some images contain dark aberrations. These are not proteins, so do not mark them.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/CWruYrG.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title',"Some proteins stick together. If you can't clearly distinguish individual proteins don't mark them.")
instructions.append(FormattedContent('<img src="http://i.imgur.com/sUAEyqf.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','Here a mass of proteins are stuck together. Do not worry about marking these regions')#, as the proteins are too difficult to distinguish.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/iMPjCbh.png" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent(''))
instructions.append(FormattedContent('<hr></hr>'))

instructions.append(FormattedContent('<font size="6">Invalid Images</font>'))
instructions.append_field('Title','Some images have problems. Do not mark any proteins in these images.')
instructions.append_field('Title','This image has a crack down the center.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/WydMOFI.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','Some images have dark splotches on them.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/lj47Snf.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','Here is a more extreme example of splotching.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/hgwfYJd.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))
instructions.append_field('Title','In this image the sample moved during image collection, creating the smudges you see here.')
instructions.append(FormattedContent('<img src="http://i.imgur.com/aN4nI6z.jpg" alt="Carbon Ring" height="600" width="600" />'))
instructions.append(FormattedContent('<hr></hr>'))

acct_overview = Overview()
acct_overview.append(FormattedContent('<font size="4">Create an account on <a target="_blank" href="https://www.zooniverse.org/accounts/register"> Zooniverse </a>. A valid email is preferred but not essential. You will know when your account is made when you see your username in the upper right-hand corner of the zooniverse page like in the image below: </font>'))
acct_overview.append(FormattedContent('<img src="http://i.imgur.com/lFJjN17.png?1" alt="Username Location" height="110" width="606" />'))


test_ac1 = QuestionContent()
test_ac1.append_field('Title','In the following section we will be asking you to verify whether particles we have marked agree with the rules above.')
test_ac1.append_field('Title','If none of the particles are valid or the image is bad, choose only the fourth option')
#test_ac1.append_field('Text','Which of the following particles are correctly marked?')
test_ac1.append(FormattedContent('<img src="http://i.imgur.com/mA5iGsO.png" alt="Test_1" height="600" width="600" />'))

test_1 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a1 = Question(identifier='mult1',
                   content=test_ac1,
                   answer_spec=AnswerSpecification(test_1),
                   is_required=True)

test_ac2 = QuestionContent()
#test_ac2.append_field('Text','Which of the following particles are correctly marked?')
test_ac2.append(FormattedContent('<img src="http://i.imgur.com/magbLnm.png" alt="Test_2" height="600" width="600" />'))

test_2 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a2 = Question(identifier='mult2',
                   content=test_ac2,
                   answer_spec=AnswerSpecification(test_2),
                   is_required=True)


test_ac3 = QuestionContent()
#test_ac3.append_field('Text','Which of the following particles are correctly marked?')
test_ac3.append(FormattedContent('<img src="http://i.imgur.com/ZlqDWsW.png" alt="Test_3" height="600" width="600" />'))

test_3 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a3 = Question(identifier='mult3',
                   content=test_ac3,
                   answer_spec=AnswerSpecification(test_3),
                   is_required=True)


test_ac4 = QuestionContent()
#test_ac4.append_field('Text','Which of the following particles are correctly marked?')
test_ac4.append(FormattedContent('<img src="http://i.imgur.com/fwOqyKE.png" alt="Test_4" height="600" width="600" />'))

test_4 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a4 = Question(identifier='mult4',
                   content=test_ac4,
                   answer_spec=AnswerSpecification(test_4),
                   is_required=True)


test_ac5 = QuestionContent()
#test_ac5.append_field('Text','Which of the following particles are correctly marked?')
test_ac5.append(FormattedContent('<img src="http://i.imgur.com/E84wLEX.png" alt="Test_5" height="600" width="600" />'))

test_5 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a5 = Question(identifier='mult5',
                   content=test_ac5,
                   answer_spec=AnswerSpecification(test_5),
                   is_required=True)


test_ac6 = QuestionContent()
#test_ac6.append_field('Text','Which of the following particles are correctly marked?')
test_ac6.append(FormattedContent('<img src="http://i.imgur.com/DanvLNS.png" alt="Test_6" height="600" width="600" />'))

test_6 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a6 = Question(identifier='mult6',
                   content=test_ac6,
                   answer_spec=AnswerSpecification(test_6),
                   is_required=True)


test_ac7 = QuestionContent()
#test_ac7.append_field('Text','Which of the following particles are correctly marked?')
test_ac7.append(FormattedContent('<img src="http://i.imgur.com/JbAye3k.png" alt="Test_4" height="600" width="600" />'))

test_7 = SelectionAnswer(min=1,max=3,style='checkbox',selections=[('A','1'),('B','2'),('C','3'),('None / Bad image','4')],type='text')
test_a7 = Question(identifier='mult7',
                   content=test_ac7,
                   answer_spec=AnswerSpecification(test_7),
                   is_required=True)


acct_c = QuestionContent()
acct_c.append_field("Title","Enter your Zooniverse username, please make sure it's accurate (your qualification will be rejected otherwise!)")
acct_fta1 = FreeTextAnswer(num_lines=1)
acct_q1 = Question(identifier="username",
              content=acct_c,
              answer_spec=AnswerSpecification(acct_fta1),
              is_required=True)


test_form = QuestionForm()
test_form.append(instructions)
test_form.append(test_a1)
test_form.append(test_a2)
test_form.append(test_a3)
test_form.append(test_a4)
test_form.append(test_a5)
test_form.append(test_a6)
test_form.append(test_a7)
#test_form.append(acct_overview)
#test_form.append(acct_q1)

title = 'Protein Picker Expert'
description = 'Qualification for work on Protein Picker HITs'

qual = mtc.create_qualification_type('Protein Picker Expert','Qualification for working on Protein Picker HITs','Active',test=test_form,test_duration=3600,retry_delay=180)

print qual

