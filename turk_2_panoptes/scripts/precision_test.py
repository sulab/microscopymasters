#!/usr/bin/python

import os
import json
import csv
import random
import copy

import expert_vote
import voted_performance

def main():
    
    with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/data/zoo_conv.json",'r') as in_file:
        conv_data = json.load(in_file)

    with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/turk2zoo_conv.json",'r') as in_file:
        zoo2turk = json.load(in_file)

    out_data = []

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/'):
        try:
            gold_id = conv_data[subj_dir]
        except KeyError:
            continue

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:

            reader = csv.reader(in_file,delimiter='\t')
            gold_data = map(lambda x: [float(x[0]),float(x[1])],reader)


        anno_data = []

        for user_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/%s/' % subj_dir):
           
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/%s/%s' % (subj_dir,user_file),'r') as in_file:
                reader = csv.reader(in_file,delimiter='\t')
                marks = map(lambda x: expert_vote.my_point([float(x[0]),float(x[1])],1),reader)
            anno_data.append(marks)

        for cnt in xrange(10):

            random.shuffle(anno_data)
            agg_pts = []
            for i in xrange(len(anno_data)):

                agg_pts = expert_vote.two_vote(agg_pts,copy.deepcopy(anno_data[i]),20)
                for v in range(i):

                    voted_pts = filter(lambda x: x.num > v,agg_pts)
                    voted_coords = map(lambda x: x.coord,voted_pts)
                    performance_vec = voted_performance.gold_eval(voted_coords,gold_data)

                    zooID = zoo2turk[subj_dir]

                    entry = [zooID,cnt,i,v]
                    entry.extend(performance_vec)

                    out_data.append(entry)
                
                

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/precision_tests.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['subj_id','cnt','num_annotators','v','tp','fp','fn'])

        for entry in out_data: writer.writerow(entry)



                
                            

if __name__ == '__main__':
    main()
