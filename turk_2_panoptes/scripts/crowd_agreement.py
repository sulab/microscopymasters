#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num

def main():

    agree_dat = []

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/'):
        subj_entry = []
        quality_entry = []
        
        for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                user_id = anno_file
                
                reader = csv.reader(in_file,delimiter='\t')
                entry = []
                
                for line in reader:
                    conv_line = map(lambda x: float(x), line)
                    # entry.append(conv_line[0:2])
                    entry.append(conv_line[0:2]) 

            subj_entry.append([user_id,entry])
            
        for comb in itertools.combinations(subj_entry,2):

            agreement = inter_agreement(comb[0][1],comb[1][1])
            entry = [subj_dir,comb[0][0],comb[1][0],agreement[0],agreement[1]]
            agree_dat.append(entry)
        

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/inter_user_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['subj_id','user_id1','user_id2','union','intersection'])
        
        for entry in agree_dat:
            writer.writerow(entry)

def inter_agreement(datapts,goldpts):
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0)
    elif gold_n == 0:
        return (0,exp_n)
    elif exp_n == 0:
        return (0,gold_n)
        
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp = matches
    
    return (tp,exp_n+gold_n)

if __name__ == '__main__':
    main()

