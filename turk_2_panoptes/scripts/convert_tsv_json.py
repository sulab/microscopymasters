#!/home/jbrugg/env/bin/python
import json
import csv

data = {}

with open('data/AMT_ZOO.tsv','r') as in_file:
    reader = csv.reader(in_file,delimiter='\t')

    for line in reader:
        data[line[0]] = [line[1]]

with open('data/AMT_ZOO.json','w') as out_file:
    json.dump(data,out_file)


