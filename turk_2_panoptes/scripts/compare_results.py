#!/usr/bin/python

import numpy as np
from scipy import spatial
from PIL import Image, ImageDraw

import json
import argparse
import csv

def shell_data(data_dict):
    shelled_dict = {}
    for key, value in data_dict.iteritems():
        shelled_dict[key] = map(lambda x: [float(i) for i in x], value)
    return shelled_dict

def gold_eval(datapts,goldpts):
    
    agreement = {}

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 or exp_n == 0:
        #return (1.0,1.0,1.0)
        return (0,0,0)

    #print goldpts
    #print datapts
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    '''if tp != 0:

        recall = float(tp) / (tp+fn)

        precision = float(tp) / (tp+fp)
        
        fscore = 2*recall*precision/(recall+precision)
        
    else:
        recall = 0
        precision = 0
        fscore = 0'''


    return (tp,fp,fn)


def draw_circles(points,img_file,out_file):
    with Image.open(img_file).convert("RGB") as img:
        
        drawer = ImageDraw.Draw(img)

        colors = ['red','blue','orange','purple','pink','yellow']

        for point_set in points:
            color = colors.pop(0)
            for point in point_set:
                drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],fill=None,outline=color)

        del drawer
        img.save(out_file)

def draw_gold(points,filename):
    with Image.open(filename) as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        for point in points:
            drawer.ellipse([point[0]-4,point[1]-4,point[0]+4,point[1]+4],fill=None,outline=(255,255,51,255))
        del drawer
        img.save(filename)

def mask_img(img,coords):

    new_coords = (int(coords[0]*.748),478-int(coords[1]*.748))

    try:
        val = img.getpixel(new_coords)
    except IndexError:
        return False

    if val > 0:
        return False
    else:
        return True

def main():

    WORK_DIR = "/Users/Jake/Documents/Projects/markTheCrowd/turk_2_panoptes/"
    
    parser = argparse.ArgumentParser(description="Point to an automatic picker json file and compare results")
    parser.add_argument('auto_dat',type=str,help='Location of results file')
    parser.add_argument('out_dir',type=str,help="location of output")

    args = parser.parse_args()
    
    auto_file = args.auto_dat

    with open(auto_file,'r') as in_file:
        raw_auto_dat = json.load(in_file)
    auto_dat = shell_data(raw_auto_dat)

    with open(WORK_DIR+'user_results/crowd_results.json','r') as in_file:
        raw_crowd_dat = json.load(in_file)
    crowd_dat = shell_data(raw_crowd_dat)

    with open(WORK_DIR+'data/gold_dat.json','r') as in_file:
        raw_gold_dat = json.load(in_file)
    gold_dat = shell_data(raw_gold_dat)
    
    with open(WORK_DIR+'data/code_img_file.json','r') as in_file:
        file_data = json.load(in_file)

    crwdAutoDat = []
    goldAutoDat = []


    for img_id,entry in crowd_dat.iteritems():

        in_file = WORK_DIR+"data/img_files/"+file_data[img_id]
        out_file = WORK_DIR+args.out_dir+"pics/"+img_id+".jpg"

        voted_entry = filter(lambda x: x[2] > 0, entry)
        crwd_entry = map(lambda x: [x[0],x[1]] , voted_entry)

        auto_entry = auto_dat.get(img_id,"NO")
        if auto_entry == "NO":
            continue

        mask_file = WORK_DIR +"../masks/"+file_data[img_id][:-4] + '_mask.png'
        try:
            with Image.open(mask_file) as img:
                crwd_entry = filter(lambda x: mask_img(img,x), crwd_entry)
                auto_entry = filter(lambda x: mask_img(img,x), auto_entry)
        except IOError:
            pass

        draw_circles([auto_entry,crwd_entry],in_file,out_file)
        
        results = gold_eval(crwd_entry,auto_entry)
        try:
            agreement = float(results[0]) / sum(results)
        except ZeroDivisionError:
            agreement = "NaN"
        crwdAutoDat.append((img_id,agreement,results[0],results[1],results[2]))
        
        gold_entry = gold_dat.get(img_id,"NO")
        if not gold_entry == "NO":
            gold_results = gold_eval(auto_entry,gold_entry)
            goldAutoDat.append((img_id,gold_results[0],gold_results[1],gold_results[2]))

            draw_gold(gold_entry,out_file)

    

    with open(WORK_DIR+args.out_dir+"gold_results.tsv",'w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        for line in goldAutoDat:
            writer.writerow(line)

    with open(WORK_DIR+args.out_dir+"crowd_agreement.tsv",'w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        for line in crwdAutoDat:
            writer.writerow(line)

    


if __name__ == "__main__":
    main()
