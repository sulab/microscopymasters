#!/home/jbrugg/env/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
from rauth import OAuth2Service
import requests
import urllib,urllib2
import csv
import copy
import time

import sys
import json

import numpy as np
from scipy import spatial
import itertools

def gold_eval(datapts,goldpts):
    
    #let's do this by a total evaluation... like look over the whole corpus. 

    agreement = {}

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 or exp_n == 0:
        return (1.0,1.0,1.0)
        #return (0,0,0)

    #print goldpts
    #print datapts
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    if tp != 0:

        recall = float(tp) / (tp+fn)

        precision = float(tp) / (tp+fp)
        
        fscore = 2*recall*precision/(recall+precision)
        
    else:
        recall = 0
        precision = 0
        fscore = 0


    return (recall,precision,fscore)



def check_HIT(line,req_header,id_data,g_data,class_data,id_conversion):

    user_req = requests.get("https://panoptes.zooniverse.org/api/users?login=%s" % line[0],headers=req_header)
    user_dat =  user_req.json()

    user_id = user_dat['users'][0]['id']

    #print id_data
    #subject_ids = [id_conversion[line[2][3:-2]]]
    subject_ids = line[2][3:-2]

    subj_done = 0

    test_case = None
    f_score = None
    
    completed_subs = {}

    user_data = {}

    for anno in class_data:
        if anno['links'].get('user','') != user_id:
            continue

        elif anno['links']['subjects'][0] in subject_ids:
            
            subj = anno['links']['subjects'][0]
            
            #ncompleted_subs.append(subj)
            
            marks = map(lambda x: [x['x'],x['y']], anno['annotations'][0]['value'])
            user_data[subj] = marks

            '''curr = completed_subs.get(subj,0)
            if curr > len(marks):
		continue
            completed_subs[subj] = max(curr,len(marks))'''
            
            if subj in g_data.keys():

                land_dat = g_data[subj]

                results = gold_eval(marks,land_dat)
                f_score = results[2]
                
                if f_score < 0.05:
                    test_case = False
                else:
                    test_case = True

    #zero_ents = filter(lambda x: x == 0, completed_subs.values())
    #strikes = len(zero_ents) + 10 - len(completed_subs)

    if test_case == None:
        test_case = True
    if f_score == None:
        f_score = 'NA'

    #print strikes
    #print f_score
    #test_case = False
    #strikes = 0
    entry = [line[0],line[1],line[2],f_score]
    if len(user_data) == 0:
        with open('user_results/rejections2.tsv','w') as out_file:
            tsv_writer = csv.writer(out_file,delimiter='\t')
            tsv_writer.writerow(entry)

        return False,'We were unable to find your results on Zooniverse and are therefore rejecting your work. In the future remember to click DONE before submitting the HIT. If you have any grievances please feel free to contact us.'

    subject = id_conversion[user_data.keys()[0]]
    with open('user_results/acceptions2.tsv','aw') as out_file:
        tsv_writer = csv.writer(out_file,delimiter='\t')
        tsv_writer.writerow(entry)

    result_file = 'user_results/annotations/'+subject+'/'+line[0]+'.tsv'
            
    with open(result_file,'w') as out_file:
        tsv_writer = csv.writer(out_file,delimiter='\t')
        for entry in user_data[subject_ids]:
            tsv_writer.writerow(entry)
    return True,''
    
    '''elif strikes > 2:
	entry = [line[0],line[1],line[2],strikes,f_score]
	with open('user_results/rejections.tsv','aw') as out_file:
            tsv_writer = csv.writer(out_file,delimiter='\t')
            tsv_writer.writerow(entry)
        for subject in user_data.keys():        
            result_file = 'user_results/rej_annotations/'+subject+'/'+line[0]+'.tsv'
            with open(result_file,'w') as out_file:
                tsv_writer = csv.writer(out_file,delimiter='\t')
                for entry in user_data[subject]:
                    tsv_writer.writerow(entry)
        return False,'You did not complete all of the images on Zooniverse, if you think you have been wrongly accused you can contact us at spudman.jobs@gmail.com'

    elif not test_case:
        entry = [line[0],line[1],line[2],strikes,f_score]
        with open('user_results/rejections.tsv','aw') as out_file:
            tsv_writer = csv.writer(out_file,delimiter='\t')
            tsv_writer.writerow(entry)
        for subject in user_data.keys():        
            result_file = 'user_results/rej_annotations/'+subject+'/'+line[0]+'.tsv'
            with open(result_file,'w') as out_file:
                tsv_writer = csv.writer(out_file,delimiter='\t')
                for entry in user_data[subject]:
                    tsv_writer.writerow(entry)
        return False,'Your accuracy was not deemed to be valid, we suspect you were either clicking randomly or using an automated system. If you think you have been wrongly accused you can contact us at spudman.jobs@gmail.com'

    else:
        entry = [line[0],line[1],line[2],strikes,f_score]
        with open('user_results/rejections.tsv','aw') as out_file:
            tsv_writer = csv.writer(out_file,delimiter='\t')
            tsv_writer.writerow(entry)
        for subject in user_data.keys():        
            result_file = 'user_results/rej_annotations/'+subject+'/'+line[0]+'.tsv'
            with open(result_file,'w') as out_file:
                tsv_writer = csv.writer(out_file,delimiter='\t')
                for entry in user_data[subject]:
                    tsv_writer.writerow(entry)
        return False,'You neither completed all the images nor were you accurate enough. We suspect you were not honest in your work, and have rejected your work. If you feel you have been wrongly accused please contact us at spudman.jobs@gmail.com'
    '''
    
        

def main():

    ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
    SECRET_KEY = '7gF2et3y3yC2dHWbOtjI/AyRKXzy8Y9/wv5k5Wk4'
    HOST = 'mechanicalturk.amazonaws.com'

    mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                          aws_secret_access_key=SECRET_KEY,
                          host=HOST)

    my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
    my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

    panoptes = OAuth2Service(
        client_id=my_id,
        client_secret=my_secret,
        name='panoptes',
        authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
        access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
        base_url="https://panoptes.zooniverse.org/"
        )

    params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}
    
    r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

    token = r.json()["access_token"]

    header_get = {'Accept': 'application/vnd.api+json; version=1',
                  'Content-Type': 'application/json',
                  'Authorization' : 'Bearer %s' % token}


    with open('data/glander.json','r') as in_file:
        glander_data = json.loads(in_file.read())
        
    with open('data/glander_conv.json','r') as in_file:
        id_conv = json.loads(in_file.read())

    new_g_data = {}

    for key,value in glander_data.iteritems():
        try:
            new_g_data[id_conv[key]] = value
        except:
            pass
        

    '''with open('data/id_set.json','r') as in_file:
        id_data = json.load(in_file)'''

    with open('data/id_conversion.json','r') as in_file:
        conv_2016 = json.load(in_file)
    
    with open('data/amt_2016_code_subj.json','r') as in_file:
        id_data = json.load(in_file)

        
    '''for i in range(186,237):
        class_req = requests.get("https://panoptes.zooniverse.org/api/classifications?sort=created_at&page=%d" % i, headers=header_get)
        #print class_req.text
        class_data.extend(class_req.json()['classifications'])
        time.sleep(0.1)'''

    with open('data/classifications.json','r') as in_file:
        class_data = json.load(in_file)['classifications']

    with open('data/assignments_2_verify.tmp','r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        for line in reader:
            results = check_HIT(line,header_get,id_data,new_g_data,class_data,conv_2016)
            if results[0]:
                mtc.approve_assignment(line[1])
                pass
            else:
                #mtc.reject_assignment(line[1],results[1])
                pass




if __name__ == "__main__":
    main()
