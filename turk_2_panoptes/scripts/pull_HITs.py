#!/home/jbrugg/env/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
from rauth import OAuth2Service
import requests
import urllib,urllib2
import csv
import copy
import time

import sys
import json




if __name__ == "__main__":

    ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
    SECRET_KEY = '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'
    HOST = 'mechanicalturk.amazonaws.com'

    mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                          aws_secret_access_key=SECRET_KEY,
                          host=HOST)

    hits = mtc.get_all_hits()

    '''with open('data/rev_code_set.json','r') as in_file:
        code_data = json.loads(in_file.read())
    
    with open('data/id_set.json','r') as in_file:
        id_data = json.loads(in_file.read())'''

    with open("data/amt_2016_code_subj.json",'r') as in_file:
        code_data = json.load(in_file)
        
    out_data = []
    rej_data = []

    for hit in hits:

        hit_id = hit.HITId

        hit_ass = mtc.get_assignments(hit_id)
        

        for ass in hit_ass:
            
            status = ass.AssignmentStatus
            
            ass_id = ass.AssignmentId

            if status == 'Submitted':

                entry = [copy.deepcopy(ass.WorkerId)]
                
                entry.append(ass_id)
                
                answer_form = ass.answers[0][0]
                answers = answer_form.fields[0]
                answer_vec = answers.split('|')
		answer = answer_vec[0].strip(' ').strip('\t')
                
		subj_set = code_data.get(answer,'ERROR')
		if subj_set == "ERROR":
			entry.append(answer)
			rej_data.append(entry)
		else:
			entry.append(subj_set)
			out_data.append(entry)


    
    with open('data/assignments_2_verify.tmp','w') as out_file:
        dat_writer = csv.writer(out_file,delimiter='\t')
        for line in out_data:
            dat_writer.writerow(line)

    rejection_file = 'data/rejections/'+time.strftime("%H-%M-%S-%d-%m-%Y")+"_rejection.tsv"
    with open(rejection_file,'w') as out_file:
	dat_writer = csv.writer(out_file,delimiter='\t')
	for line in rej_data:
		dat_writer.writerow(line)
