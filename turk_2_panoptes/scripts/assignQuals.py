#!/home/jbrugg/env/bin/python

from boto.mturk.connection import MTurkConnection
from boto.mturk.question import QuestionContent,Question,QuestionForm,Overview,AnswerSpecification,SelectionAnswer,FormattedContent,FreeTextAnswer
from rauth import OAuth2Service
import requests
import urllib,urllib2
import csv

import json

def verify_results(answers):
    
    answer_key = [[2],[1],[4],[1],[2],[2,3],[4]]

    #acc =  len([i for i,j in zip(answers,answer_key) if i == j])
    acc = 0
    
    answer_dict = {1:"A",2:"B",3:"C",4:"No proteins / Bad Image"}
    
    response = []

    for i in xrange(7):
        if answers[i] == answer_key[i]:
            acc += 1
        else:
            correct_answer = map(lambda x: answer_dict[x], answer_key[i])
            correct_str = ' & '.join(correct_answer)
            response.append("The right answer to "+str(i+1) + " is " + correct_str)
            
    feedback =  'Your score on the test was %d/7, we require a minimum of 5/7. Try retaking the test, here are the mistakes you made. ' % acc
    feedback += '. '.join(response)

    if acc > 4:
        return True, '',acc
    else:
        return False, feedback,acc



def create_panoptes_acct(turkID):
    acct_header = {'Content-Type': 'application/json',
              'Accept': 'application/json'}
    
    payload = {
        'user':
            {
            'login':'%s' % turkID,
            'email': 'sulabzooniverseacctrepo+%s@gmail.com' % turkID,
            'password': 'letsturk!',
            'credited_name':'AMT User %s' % turkID,
            'global_email_communication':False,
            'beta_email_communication':False,
            'project_email_communication':False,
            'project_id':292
            }
        }

    user_post = requests.post("https://www.zooniverse.org/users",headers=acct_header,data=json.dumps(payload))

    return user_post.status_code, user_post.text
    

'''my_id = 'fd88d6f3481e322d382efd8e3e99284c267e0f7b76ac664e54f7f7cc1a03ea6a'
my_secret = 'cc3895754dd9bed69ace804970582eb4bf80a40309747ec5f155f85f08169518'

panoptes = OAuth2Service(
    client_id=my_id,
    client_secret=my_secret,
    name='panoptes',
    authorize_url="https://panoptes.zooniverse.org/oauth/authorize",
    access_token_url="https://panoptes.zooniverse.org/oauth/access_token",
    base_url="https://panoptes.zooniverse.org/"
    )

params = {'grant_type':'client_credentials','client_id':my_id,'client_secret':my_secret}

r = requests.post("https://panoptes.zooniverse.org/oauth/token",params=params)

token = r.json()["access_token"]
'''

ACCESS_ID = 'AKIAISFIFOVDTUA47Z7Q'
SECRET_KEY = '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'

QUAL_ID = '3PO9K4KN95VJZY3HAQ4AFWIIH417YN'
HOST = 'mechanicalturk.amazonaws.com'

#QUAL_ID = '3799W5ZNMKKJ4ODM6CZ3ATXE9W67Z1'
#HOST = 'mechanicalturk.sandbox.amazonaws.com'

mtc = MTurkConnection(aws_access_key_id=ACCESS_ID,
                      aws_secret_access_key=SECRET_KEY,
                      host=HOST)

reqs =  mtc.get_qualification_requests(QUAL_ID)

errors = []

rej_users = []

users = []

for req in reqs:
    user_ans = []

    for answer in req.answers[0]:
        user_ans.append(map(lambda x: int(x), answer.fields))

    passed, reason, acc = verify_results(user_ans)

    if passed:
        status,text = create_panoptes_acct(req.SubjectId)
        
        if status != 201:
            errors.append((req.QualificationRequestId,req.SubjectId,text))
        else:
            mtc.grant_qualification(req.QualificationRequestId)
            users.append((req.SubjectId,acc))

    else:
        rej_users.append((req.QualificationRequestId,acc,req.SubjectId,user_ans,reason))
        mtc.reject_request(str(req.QualificationRequestId),reason)


with open('data/acct_rejections2.tsv','a') as out_file:
    writer = csv.writer(out_file,delimiter="\t")
    for user in errors:
        writer.writerow(user)

with open('data/qual_rejections2.tsv','a') as out_file:
    writer = csv.writer(out_file,delimiter='\t')
    for user in rej_users:
        writer.writerow(user)

with open('data/qual_acceptions2.tsv','a') as out_file:
    writer = csv.writer(out_file,delimiter='\t')
    for user in users:
        writer.writerow(user)

