#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num

def gold_eval(datapts,goldpts):
    

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0,0)
    elif gold_n == 0:
        return (0,exp_n,0)
    elif exp_n == 0:
        return (0,0,gold_n)
        
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    #matches = len(filter(lambda x: x <= 25,dist_mins))
    matches = len(filter(lambda x: x <= 10,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    return (tp,fp,fn)

        
with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/data/zoo_conv.json",'r') as in_file:
    conv_data = json.load(in_file)

with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/turk2zoo_conv.json",'r') as in_file:
    zoo2turk = json.load(in_file)
    
    
perf_data = []

for subj_id in os.listdir("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/user_results/annotations/"):

    try:
        gold_id = conv_data[subj_id]
    except KeyError:
        continue
    
    gold_data = []

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
        reader = csv.reader(in_file,delimiter='\t')
        
        for line in reader:
            gold_data.append(map(lambda x: float(x), line))

    for i in range(10):

        vote_data = []
        
        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_votes/vote_%s_%d.tsv' % (subj_id,i),'r') as in_file:
            reader = csv.reader(in_file,delimiter='\t')

            for line in reader:
                vote_data.append(map(lambda x: float(x), line))
        
        for v in range(0,7):

            thresh_data = filter(lambda x: x[2] > v,vote_data)
            thresh_pts = map(lambda x: [x[0],x[1]],thresh_data)

            performance_vec = gold_eval(thresh_pts,gold_data)

            subjZooID = zoo2turk[subj_id] 

            entry = [subjZooID,i,v]
            entry.extend(performance_vec)

            perf_data.append(entry)
        

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/turk_2_panoptes/analysis/cut_voted_performance.tsv','w') as out_file:
    writer = csv.writer(out_file,delimiter='\t')
    writer.writerow(['subj_id','iter','v','tp','fp','fn'])

    for entry in perf_data:
        writer.writerow(entry)
