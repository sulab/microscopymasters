#!/usr/bin/python

import json
import csv
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num

def find_matches(mat,fewer_rows,dist_thresh):
    #rows
    row_mins = []
    for i in xrange(mat.shape[0]):
        inds = np.where(mat[i,:] == min(mat[i,:]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        row_mins.append(ind)

    col_mins = []
    for i in xrange(mat.shape[1]):
        inds = np.where(mat[:,i] == min(mat[:,i]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]

            col_mins.append(ind)


    matches = []

    if fewer_rows:
        for i in xrange(len(row_mins)):
            col_ind = row_mins[i]
            if mat[i,col_ind] < dist_thresh:
                matches.append((i,col_ind,mat[i,col_ind]))
    else:
        for i in xrange(len(col_mins)):
            row_ind = col_mins[i]
            if mat[row_ind,i] < dist_thresh:
                matches.append((row_ind,i,mat[row_ind,i]))

    return sorted(matches,key= lambda x: x[2])

def two_vote(points1,points2,dist_thresh):

    results = []
    
    n = len(points1)
    m = len(points2)

    diff = n-m
        
    if n == 0 or m == 0:
        results = []
        results.extend(points1)
        results.extend(points2)
        return results

    dist_mat = spatial.distance_matrix([pt.coord for pt in points1],[pt.coord for pt in points2])
    
    points = find_matches(dist_mat,(n<m),dist_thresh)

    for point in points:

        point1 = points1[point[0]]
        point2 = points2[point[1]]
        
        points1[point[0]] = None
        points2[point[1]] = None

        if point1 == None:
            results.append(point2)
        elif point2 == None:
            results.append(point1)
        else:
            avg_point = [(point1.coord[0]+point2.coord[0])/2,(point1.coord[1]+point2.coord[1])/2]
        
            results.append(my_point(avg_point,point1.num+point2.num))
        

    f_pts1 = filter(lambda x: x is not None,points1)
    f_pts2 = filter(lambda x: x is not None,points2)

    results.extend(f_pts1)
    results.extend(f_pts2)

    return results

def n_vote(expert_data,dist_thresh):

    random.shuffle(expert_data)
        
    first_exp = expert_data.pop()

    while expert_data:
        next_exp = expert_data.pop()
        join_exp = two_vote(first_exp, next_exp,dist_thresh)
        first_exp = join_exp
            
    return first_exp

        
def resolve_duplicates(user_id,subj_id,data):

    cnt = 1

    conv_data = []
    
    for entry in data:
        if not os.path.exists('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/%s/' % (user_id,subj_id)):
            os.makedirs('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/%s/' % (user_id,subj_id))

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/%s/%02d.tsv' % (user_id,subj_id,cnt),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            conv_entry = []
            for mark in entry:
                writer.writerow(mark)
                conv_entry.append(my_point(mark,1))
            conv_data.append(conv_entry)

        cnt += 1
    
    combined_data = n_vote(copy.deepcopy(conv_data),20)
    
    if not os.path.exists('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/' % subj_id):
        os.makedirs('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/' % subj_id)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s.tsv' % (subj_id,user_id),'w') as out_file:

        # print subj_id,user_id
        
        writer = csv.writer(out_file,delimiter='\t')
        for mark in combined_data: writer.writerow(mark.coord)
    
    return

gold_data = {}

with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/raw_gold.csv','r') as in_file:
    reader = csv.reader(in_file,delimiter=',')

    for line in reader:

        user = line[1]
        user = user.strip('00')

        user_entry = gold_data.get(user,{})
        
        anno = json.loads(line[11])[0]
        vals = anno['value']
        marks = map(lambda x: [x['x'],x['y']], vals)

        subject = json.loads(line[12])
        subj_id = subject.keys()[0]

        subj_entry = user_entry.get(subj_id,[])
        subj_entry.append(marks)

        user_entry[subj_id] = subj_entry
        gold_data[user] = user_entry
        


for user_id in gold_data.keys():
    for subj_id in gold_data[user_id].keys():

        entry = gold_data[user_id][subj_id]

        if len(entry) > 1:
            resolve_duplicates(user_id,subj_id,entry)
        else:
            if not os.path.exists('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s' % subj_id):
                os.makedirs('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s' % subj_id)

            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s.tsv' % (subj_id,user_id),'w') as out_file:
                writer = csv.writer(out_file,delimiter='\t')
                for mark in entry[0]: writer.writerow(mark)
            
