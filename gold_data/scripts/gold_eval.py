#!/usr/bin/python

import json
import csv
import os
import itertools

from gold_vote import *

def inter_agreement(datapts,goldpts):
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0)
    elif gold_n == 0:
        return (0,exp_n)
    elif exp_n == 0:
        return (0,gold_n)
        
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp = matches
        
    return (tp,exp_n+gold_n)


def main():
    #### inter_agreement
    out_data = []

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/'): 
        subj_entry = {}
        
        for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s' % (subj_dir,anno_file), 'r') as in_file:

                reader = csv.reader(in_file,delimiter='\t')
                entry = []
                
                for line in reader:
                    conv_line = map(lambda x: float(x), line)
                    entry.append(my_point(conv_line,1))

            # subj_entry.append(entry)

            subj_entry[anno_file[0:-4]] = entry
            # subj_entry[anno_file.rstrip('.tsv')] = entry

        for user_comb in itertools.combinations(subj_entry.keys(),2):
            entry1 = subj_entry[user_comb[0]]
            entry2 = subj_entry[user_comb[1]]

            # voted_pts = inter_agreement(entry1,entry2)
            voted_pts = two_vote(copy.deepcopy(entry1),copy.deepcopy(entry2),15)
            intersection = len(filter(lambda x: x.num > 1, voted_pts))
            union = len(voted_pts)

            data_entry = [subj_dir,user_comb[0],user_comb[1],intersection,union,len(entry1),len(entry2)]

            out_data.append(data_entry)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/inter_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['subj_id','user_1','user_2','intersection','union','length_1','length_2'])
        for entry in out_data:
            writer.writerow(entry)

    #### intra-agreement

    out_data = []

    voted_comparison = []

    for user_id in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/'):
        for subj_id in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/' % user_id):

            dupl_data = []

            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s.tsv' % (subj_id,user_id),'r') as in_file:
                reader = csv.reader(in_file,delimiter='\t')
                vote_data = map(lambda x: my_point([float(x[0]),float(x[1])],1),reader)
            
            for iter_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/%s/' % (user_id,subj_id)):

                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/duplicate_gold/%s/%s/%s' % (user_id,subj_id,iter_file),'r') as in_file:
                    reader = csv.reader(in_file,delimiter='\t')

                    marks = map(lambda x: my_point([float(x[0]),float(x[1])],1), reader)
                    # intersection,union = two_vote(marks,vote_data)
                    voted_pts = two_vote(copy.deepcopy(marks),copy.deepcopy(vote_data),15)
                    union = len(voted_pts)
                    intersection = len(filter(lambda x: x.num > 1, voted_pts))
                    voted_comparison.append([user_id,subj_id,intersection,union,len(marks),len(vote_data)])
                    
                    dupl_data.append(marks)
                
            for comb in itertools.combinations(dupl_data,2):
                # intersection, union = inter_agreement(comb[0],comb[1])
                voted_pts = two_vote(copy.deepcopy(comb[0]),copy.deepcopy(comb[1]),15)
                union = len(voted_pts)
                intersection = len(filter(lambda x: x.num > 1, voted_pts))

                data_entry = [user_id,subj_id,intersection,union,len(comb[0]),len(comb[1])]

                out_data.append(data_entry)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/intra_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['user_id','subj_id','intersection','union','len_1','len_2'])

        for entry in out_data:
            writer.writerow(entry)
        
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/intra_net_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['user_id','subj_id','intersection','union','indiv_len','union_len'])

        for entry in voted_comparison:
            writer.writerow(entry)


                                    
    return

if __name__ == '__main__':
    main()
