#!/usr/bin/python

import json
import csv
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

def get_intra_distances(marks):

    if len(marks) == 0:
        return []

    dist_mat = spatial.distance_matrix(marks,marks)

    min_dists = []

    for i in xrange(dist_mat.shape[0]):

        row = filter(lambda x: x> 0.0,dist_mat[i,:])

        if len(row) > 0:
            closest_pt = min(row)

            min_dists.append(closest_pt)

    return min_dists

def get_inter_distances(annos):

    min_dists = []

    annos = filter(lambda x: len(x) > 0, annos)

    for comb in itertools.combinations(annos,2):

        if len(comb[0]) < len(comb[1]):

            dist_mat = spatial.distance_matrix(comb[0],comb[1])

        else:

            dist_mat = spatial.distance_matrix(comb[1],comb[0])

        for i in xrange(dist_mat.shape[0]):

            min_dists.append(min(dist_mat[i,:]))

    return min_dists

def main():

    intra_data = []
    inter_data = []

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/'):

        anno_data = []

        for user_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s' % (subj_dir,user_file),'r') as in_file:
                reader = csv.reader(in_file,delimiter='\t')
                marks = map(lambda x: [float(x[0]),float(x[1])],reader)
                anno_data.append(marks)

                intra_data.extend(get_intra_distances(marks))

        inter_data.extend(get_inter_distances(anno_data))

        
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/min_intra_dists.tsv','w') as out_file:

        for line in intra_data: out_file.write('%f\n' % line)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/analysis/min_inter_dists.tsv','w') as out_file:

        for line in inter_data: out_file.write('%f\n' % line)
        

    return

if __name__ == '__main__':
    main()
