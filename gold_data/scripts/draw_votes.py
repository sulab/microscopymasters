#!/usr/bin/python

import json
import os
import csv

from PIL import Image, ImageDraw

def main():
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json','r') as in_file:
        gold_conv = json.load(in_file)
        
    gold2zoo = {}
    for key, val in gold_conv.iteritems(): gold2zoo[val] = key
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/img_key.json','r') as in_file:
        img_dat = json.load(in_file)
        
    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/'):
        
        zooID = gold2zoo[subj_dir]
        
        # voted data
        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % subj_dir,'r') as in_file:
            reader = csv.reader(in_file,delimiter='\t')
            vote_data = []
            
            for line in reader:
                vote_data.append(map(lambda x: float(x), line))
                
        # user annotations
        anno_data = {}
            
        for user_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/' % subj_dir):
            entry = []
            if user_file != 'glander.tsv':
                continue
            
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/raw_data/processed_gold/%s/%s' % (subj_dir,user_file),'r') as in_file:
                
                reader = csv.reader(in_file,delimiter='\t')
                for line in reader:
                    entry.append(map(lambda x: float(x), line))
                    
            # anno_data.append(entry)
            anno_data[user_file.replace('.tsv','')] = entry
            
        # Start drawing here
        
        #Draw individual votes first
        
        init_img = '/Users/Jake/Documents/Projects/crowdProj/ProteasomeLid/lid_jpg/%s' % img_dat[zooID]
        # indiv_votes = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/marked_imgs/indiv_votes/%s.jpg' % zooID
        indiv_votes = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/tmp/glander_imgs/%s.jpg' % subj_dir

        if len(anno_data) > 0:
            draw_circles(anno_data,init_img,indiv_votes)
        
        #Now voted
        
        # voted_img = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/marked_imgs/voted_imgs/%s.jpg' % zooID
        # draw_votes(vote_data,init_img,voted_img)
        
    return

def draw_gold(points,filename, out_file):
    with Image.open(filename) as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        for point in points:
            drawer.ellipse([point[0]-4,point[1]-4,point[0]+4,point[1]+4],fill=None,outline=(255,255,51,255))
        del drawer
        img.save(out_file)

def draw_votes(points,img_file,out_file):
    with Image.open(img_file).convert("RGBA") as img:
        drawer = ImageDraw.Draw(img,"RGBA")

        # black for orphan votes, blue-red gradient to show anything above that
        
        #color_code = {1: '#000000', 2: '#0000FF', 3: '#1500E9', 4: '#2A00D4', 5: '#3F00BF', 6: '#5500AA', 7: '#6A0094', 8: '#7F007F', 9: '#94006A', 10: '#AA0055', 11: '#BF003F', 12: '#D4002A', 13: '#E90015', 14: 'FF0000'}

        for point in points:
            drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],outline='#0000FF')

        del drawer
        img.save(out_file)

def draw_circles(points,img_file,out_file):
    with Image.open(img_file).convert("RGB") as img:
        
        drawer = ImageDraw.Draw(img)

        colors = {'saikat00':'red','mherzik00':'blue','mherzik':'blue','glander':'orange','glander00':'orange','kefauver':'purple','puchades':'pink','kimmor':'yellow','grotjahn':'gray','asong':'lime','skakuea':'green','saikat':'red'}
        
        for key in points.keys():
            color = colors[key]
            for point in points[key]:
                drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],fill=None,outline=color)

        del drawer
        img.save(out_file)
    
if __name__ == "__main__":
    main()
