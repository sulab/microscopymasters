#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

from vote import *

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num

def main():

    agree_dat = []
    quality_dat = []

    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):
        subj_entry = []
        quality_entry = []
        
        for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                user_id = in_file.next().strip('\n')
                in_file.next()
                yes_no = in_file.next().strip('\n')
                
                reader = csv.reader(in_file,delimiter='\t')
                entry = []
                
                for line in reader:
                    conv_line = map(lambda x: float(x), line)
                    entry.append(my_point(conv_line,1))

            subj_entry.append([user_id,entry])
            quality_entry.append(yes_no)

        num_yes = len(filter(lambda x: x == "Yes",quality_entry))
        num_no = len(quality_entry) - num_yes

        quality_dat.append([subj_dir,num_yes,num_no])

        for comb in itertools.combinations(subj_entry,2):

            # agreement = inter_agreement(comb[0][1],comb[1][1])

            pts1 = comb[0][1]
            pts2 = comb[1][1]

            voted_pts = two_vote(copy.deepcopy(pts1),copy.deepcopy(pts2),15)
            intersection = len(filter(lambda x: x.num > 1, voted_pts))
            union = len(voted_pts)


            entry = [subj_dir,comb[0][0],comb[1][0],intersection,union]

            agree_dat.append(entry)
        

    with open('analysis/inter_user_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['subj_id','user_id1','user_id2','union','intersection'])
        
        for entry in agree_dat:
            writer.writerow(entry)

    with open('analysis/quality_agreement.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['subj_id','num_yes','num_no'])

        for entry in quality_dat:
            writer.writerow(entry)

def inter_agreement(datapts,goldpts):
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0)
    elif gold_n == 0:
        return (0,exp_n)
    elif exp_n == 0:
        return (0,gold_n)
        
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp = matches
        

    return (tp,exp_n+gold_n)


if __name__ == "__main__":
    main()
