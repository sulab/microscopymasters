#!/usr/bin/python

import datetime
import csv

def main():

    user_times = {}
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/raw_data/user_times.csv','r') as in_file:

        reader = csv.reader(in_file,delimiter=',')

        for line in reader:
            user = line[1]
            user_entry = user_times.get(user,[])
            time_entry = line[-1]
            year = int(time_entry[0:4])
            month = int(time_entry[5:7])
            day = int(time_entry[8:10])
            hour = int(time_entry[11:13])
            minute = int(time_entry[14:16])
            second = int(time_entry[17:19])

            user_entry.append([datetime.datetime(year,month,day,hour,minute,second),line[0]])

            user_times[user] = user_entry

    time_data = []

    total_times = []
    
    for user_id in user_times.keys(): 

        entry = user_times[user_id]

        entry.sort(key=lambda x: x[0])
        
        time_diffs = [[entry[i+1][0]-entry[i][0],entry[i+1][1]] for i in range(len(entry)-1)]

        set_times = filter(lambda x: x[0] < datetime.timedelta(0,1200),time_diffs)
        for time in set_times:
            total_times.append([user_id,time[0],time[1]])

        last_ind = 0

        time_diffs = map(lambda x: x[0],time_diffs)
        
        for i in range(len(time_diffs)):
            if time_diffs[i] > datetime.timedelta(0,1200):
                
                number_classes = i-last_ind + 1
                if i-last_ind > 0:
                    average_time = 1.0 * reduce(lambda x,y: x+y,time_diffs[last_ind:i]).seconds/(i-last_ind)
                else:
                    average_time = -1
                time_since_last = time_diffs[i]

                time_data.append([user_id,number_classes,average_time,time_since_last])
                
                last_ind = i+1

        number_classes = len(time_diffs) - last_ind + 1
        if len(time_diffs) - last_ind > 0:
            average_time = 1.0 * reduce(lambda x,y: x+y,time_diffs[last_ind:]).seconds/(len(time_diffs)-last_ind)
        else:
            average_time = -1
        time_since_last = -1

        time_data.append([user_id,number_classes,average_time,time_since_last])

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/user_times.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['user_id','num_class','avg_time','time_waited'])
        for line in time_data:
            writer.writerow(line)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')
        writer.writerow(['user_id','time','class_id'])
        for line in total_times:
            writer.writerow(line)
                 

    return

if __name__ == "__main__":
    main()

