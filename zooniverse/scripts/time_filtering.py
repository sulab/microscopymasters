#!/usr/bin/python

import datetime
import csv
import json
import copy
import os

import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num



def find_matches(mat,fewer_rows,dist_thresh):
    #rows
    row_mins = []
    for i in xrange(mat.shape[0]):
        inds = np.where(mat[i,:] == min(mat[i,:]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        row_mins.append(ind)

    col_mins = []
    for i in xrange(mat.shape[1]):
        inds = np.where(mat[:,i] == min(mat[:,i]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        col_mins.append(ind)


    matches = []

    if fewer_rows:
        for i in xrange(len(row_mins)):
            col_ind = row_mins[i]
            if mat[i,col_ind] < dist_thresh:
                matches.append((i,col_ind,mat[i,col_ind]))
    else:
        for i in xrange(len(col_mins)):
            row_ind = col_mins[i]
            if mat[row_ind,i] < dist_thresh:
                matches.append((row_ind,i,mat[row_ind,i]))

    return sorted(matches,key= lambda x: x[2])

def two_vote(points1,points2,dist_thresh):

    results = []
    
    n = len(points1)
    m = len(points2)

    diff = n-m
        
    if n == 0 or m == 0:
        results = []
        results.extend(points1)
        results.extend(points2)
        return results

    dist_mat = spatial.distance_matrix([pt.coord for pt in points1],[pt.coord for pt in points2])
    
    points = find_matches(dist_mat,(n<m),dist_thresh)

    for point in points:

        point1 = points1[point[0]]
        point2 = points2[point[1]]
        
        points1[point[0]] = None
        points2[point[1]] = None

        if point1 == None:
            results.append(point2)
        elif point2 == None:
            results.append(point1)
        else:
            avg_point = [(point1.coord[0]+point2.coord[0])/2,(point1.coord[1]+point2.coord[1])/2]
        
            results.append(my_point(avg_point,point1.num+point2.num))
        

    f_pts1 = filter(lambda x: x is not None,points1)
    f_pts2 = filter(lambda x: x is not None,points2)

    results.extend(f_pts1)
    results.extend(f_pts2)

    return results

def n_vote(expert_data,dist_thresh):

    random.shuffle(expert_data)
        
    first_exp = expert_data.pop()

    while expert_data:
        next_exp = expert_data.pop()
        join_exp = two_vote(first_exp, next_exp,dist_thresh)
        first_exp = join_exp
            
    return first_exp


def gold_eval(datapts,goldpts): 
    

    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0,0)
    elif gold_n == 0:
        return (0,exp_n,0)
    elif exp_n == 0:
        return (0,0,gold_n)
        
         
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts) 
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    return (tp,fp,fn)


def main():

    class_times = {}

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/indiv_times.tsv','r') as in_file:

        in_file.next()
        reader = csv.reader(in_file,delimiter='\t')
        for line in reader:
            class_times[line[2]] = line[1]

    with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json",'r') as in_file:
        conv_data = json.load(in_file)

    for i in range(10):

        agree_dat = []
        gold_perf = []
        indiv_gold = []

        rejects = []

        for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):

            subj_entry = []

            for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):

                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:

                    time = class_times.get(anno_file[:-4],'1:00:00')
                    # if 'day' in time:
                    #     time = datetime.timedelta(1,0)
                    # else:
                    time = datetime.timedelta(0,int(time[0])*3600 + int(time[2:4])*60 + int(time[5:7]))
                        
                    user_id = in_file.next().strip('\n')
                    in_file.next()
                    yes_no = in_file.next().strip('\n')

                    if time < datetime.timedelta(0,10*i) and yes_no == 'No':
                        rejects.append(anno_file[:-4])
                        continue
                    
                    reader = csv.reader(in_file,delimiter='\t')
                    entry = []

                    for line in reader:
                        conv_line = map(lambda x: float(x), line)
                        entry.append(my_point(conv_line[0:2],1))
                subj_entry.append([user_id,entry,anno_file,yes_no])  #user_id, annotation, filename, yes/no quality
    
            if len(subj_entry) != 0:

                voted_pts = n_vote(copy.deepcopy(map(lambda x: x[1],subj_entry)),10)

                try:
                    gold_id = conv_data[subj_dir]
                    gold_data = []

                    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
                        reader = csv.reader(in_file,delimiter='\t')

                        for line in reader:
                            gold_data.append(map(lambda x: float(x), line))

                    full_pts = map(lambda x: x.coord, voted_pts)
                    performance_vec = gold_eval(full_pts, gold_data)

                    gold_entry = [subj_dir]
                    gold_entry.extend(performance_vec)
                    gold_perf.append(gold_entry)

                    for entry in subj_entry:

                        performance_vec = gold_eval(map(lambda x: x.coord,entry[1]),gold_data)

                        gold_entry = [subj_dir,entry[0]]
                        gold_entry.extend(performance_vec) #subj_id, user_id, tp, fp, fn
                        indiv_gold.append(gold_entry)

                except KeyError:
                    pass


        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/gold_perf.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','tp','fp','fn'])
            for line in gold_perf:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/indiv_gold.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','tp','fp','fn'])
            for line in indiv_gold:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/time_thresholding/%02d/rejections.tsv' % (i*10),'w') as out_file:
            for line in rejects:
                out_file.write(line+'\n')


    return


if __name__ == '__main__':
    main()
