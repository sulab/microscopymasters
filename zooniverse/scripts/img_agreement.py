#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,coord,num):
        self.coord = coord
        self.num = num

def main():

    '''
    how to determine images that need more work:
     - images with high variance in # and quality (maybe or??)
     - check if highest length agrees with quality, then compare to him
    '''

    agree_dat = []
    
    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):

        subj_entry = []
        quality_entry = []

        for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                user_id = in_file.next().strip('\n')
                in_file.next()
                yes_no = in_file.next().strip('\n')
                
                reader = csv.reader(in_file,delimiter='\t')
                entry = []
                
                for line in reader:
                    conv_line = map(lambda x: float(x), line)
                    entry.append(my_point(conv_line[0:2],1))

            subj_entry.append(entry)
            quality_entry.append(yes_no)

        num_yes = len(filter(lambda x: x == "Yes",quality_entry))
        num_no = len(quality_entry) - num_yes

        
        voted_pts = n_vote(copy.deepcopy(subj_entry),10.0)
        union_pts = len(voted_pts)

        voted_pts = filter(lambda x: x.num > 1,voted_pts)
        intersection_pts = len(voted_pts)
        
        agree_dat.append([subj_dir,num_yes*1.0/(num_yes+num_no),union_pts,intersection_pts,len(subj_entry)])

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/anno_quality_byimg.tsv','w') as out_file:
        writer = csv.writer(out_file,delimiter='\t')

        writer.writerow(['subj_id','qual_ratio','union_len','intersection_len','num_annos'])

        for line in agree_dat:
            writer.writerow(line)

    return

def find_matches(mat,fewer_rows,dist_thresh):
    #rows
    row_mins = []
    for i in xrange(mat.shape[0]):
        inds = np.where(mat[i,:] == min(mat[i,:]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        row_mins.append(ind)

    col_mins = []
    for i in xrange(mat.shape[1]):
        inds = np.where(mat[:,i] == min(mat[:,i]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        col_mins.append(ind)


    matches = []

    if fewer_rows:
        for i in xrange(len(row_mins)):
            col_ind = row_mins[i]
            if mat[i,col_ind] < dist_thresh:
                matches.append((i,col_ind,mat[i,col_ind]))
    else:
        for i in xrange(len(col_mins)):
            row_ind = col_mins[i]
            if mat[row_ind,i] < dist_thresh:
                matches.append((row_ind,i,mat[row_ind,i]))

    return sorted(matches,key= lambda x: x[2])

def two_vote(points1,points2,dist_thresh):

    results = []
    
    n = len(points1)
    m = len(points2)

    diff = n-m
        
    if n == 0 or m == 0:
        results = []
        results.extend(points1)
        results.extend(points2)
        return results

    dist_mat = spatial.distance_matrix([pt.coord for pt in points1],[pt.coord for pt in points2])
    
    points = find_matches(dist_mat,(n<m),dist_thresh)

    for point in points:

        point1 = points1[point[0]]
        point2 = points2[point[1]]
        
        points1[point[0]] = None
        points2[point[1]] = None

        if point1 == None:
            results.append(point2)
        elif point2 == None:
            results.append(point1)
        else:
            avg_point = [(point1.coord[0]+point2.coord[0])/2,(point1.coord[1]+point2.coord[1])/2]
        
            results.append(my_point(avg_point,point1.num+point2.num))
        

    f_pts1 = filter(lambda x: x is not None,points1)
    f_pts2 = filter(lambda x: x is not None,points2)

    results.extend(f_pts1)
    results.extend(f_pts2)

    return results

def n_vote(expert_data,dist_thresh):

    random.shuffle(expert_data)
        
    first_exp = expert_data.pop()

    while expert_data:
        next_exp = expert_data.pop()
        join_exp = two_vote(first_exp, next_exp,dist_thresh)
        first_exp = join_exp
            
    return first_exp


if __name__ == "__main__":
    main()
