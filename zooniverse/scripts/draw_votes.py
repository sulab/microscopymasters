#!/usr/bin/python

import json
import os
import csv

from PIL import Image, ImageDraw

def main():
    
    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json','r') as in_file:
        gold_conv = json.load(in_file)

    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/img_key.json','r') as in_file:
        img_dat = json.load(in_file)
        
    for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):

        # voted data
        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/vote_files/vote_%s.tsv' % subj_dir,'r') as in_file:
            reader = csv.reader(in_file,delimiter='\t')
            vote_data = []
            
            for line in reader:
                vote_data.append(map(lambda x: float(x), line))

        # user annotations
        anno_data = []
            
        for user_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):
            entry = []

            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,user_file),'r') as in_file:

                in_file.next() # user_id
                in_file.next() # version
                in_file.next() # Quality
                
                reader = csv.reader(in_file,delimiter='\t')
                for line in reader:
                    entry.append(map(lambda x: float(x), line))

            anno_data.append(entry)

        # gold data
        try:
            goldID = gold_conv[subj_dir]
            gold_data = []
            if_gold = True
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % goldID,'r') as in_file:
                reader = csv.reader(in_file,delimiter='\t')
                for line in reader:
                    gold_data.append(map(lambda x: float(x),line))

        except IOError:
            if_gold = False
        except KeyError:
            if_gold = False

        # Start drawing here

        #Draw individual votes first

        init_img = '/Users/Jake/Documents/Projects/crowdProj/ProteasomeLid/lid_jpg/%s' % img_dat[subj_dir]
        indiv_votes = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/indiv_votes/%s.jpg' % subj_dir
        draw_circles(anno_data,init_img,indiv_votes)

        #Now voted

        voted_img = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/voted_nogold/%s.jpg' % subj_dir
        draw_votes(vote_data,init_img,voted_img)

        gold_img = '/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/marked_imgs/voted_wgold/%s.jpg' % subj_dir
        
        if if_gold:
            draw_gold(gold_data,voted_img,gold_img)

    return

def draw_gold(points,filename, out_file):
    with Image.open(filename) as img:
        drawer = ImageDraw.Draw(img,"RGBA")
        for point in points:
            drawer.ellipse([point[0]-4,point[1]-4,point[0]+4,point[1]+4],fill=None,outline=(255,255,51,255))
        del drawer
        img.save(out_file)

def draw_votes(points,img_file,out_file):
    with Image.open(img_file).convert("RGBA") as img:
        drawer = ImageDraw.Draw(img,"RGBA")

        # black for orphan votes, blue-red gradient to show anything above that
        
        color_code = {1: '#000000', 2: '#0000FF', 3: '#1500E9', 4: '#2A00D4', 5: '#3F00BF', 6: '#5500AA', 7: '#6A0094', 8: '#7F007F', 9: '#94006A', 10: '#AA0055', 11: '#BF003F', 12: '#D4002A', 13: '#E90015', 14: 'FF0000'}

        for point in points:
            drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],outline=color_code[point[2]])

        del drawer
        img.save(out_file)

def draw_circles(points,img_file,out_file):
    with Image.open(img_file).convert("RGB") as img:
        
        drawer = ImageDraw.Draw(img)

        colors = ['red','blue','orange','purple','pink','yellow','gray','lime','green','aqua','black','cyan','Brown','Olive']

        for point_set in points:
            color = colors.pop(0)
            for point in point_set:
                drawer.ellipse([point[0]-20,point[1]-20,point[0]+20,point[1]+20],fill=None,outline=color)



        del drawer
        img.save(out_file)

    
if __name__ == "__main__":
    main()
