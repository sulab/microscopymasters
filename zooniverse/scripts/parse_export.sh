#!/bin/bash

grep ',13.34,' exports/microscopy-masters-classifications.csv > raw_data/practice_data.csv
grep ',13.32,' exports/microscopy-masters-classifications.csv >> raw_data/practice_data.csv

grep ',12.57,' exports/microscopy-masters-classifications.csv > raw_data/picker_init.csv
grep '\[986\]' exports/microscopy-masters-subjects.csv | cut -d',' -f1 | sed 's/^/;/' > raw_data/micromaster_ids.txt
grep -f raw_data/micromaster_ids.txt raw_data/picker_init.csv > raw_data/picker_data.csv
rm raw_data/picker_init.csv
rm raw_data/micromaster_ids.txt

grep ',12.67,' exports/microscopy-masters-classifications.csv >> raw_data/picker_data.csv

cut -d',' -f8 raw_data/picker_data.csv | sort >  raw_data/picker_times.txt
cut -d',' -f1,2,8 raw_data/picker_data.csv | sort -k 2,2 -t',' > raw_data/user_times.csv

# 1134629
