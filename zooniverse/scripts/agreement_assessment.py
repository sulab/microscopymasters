#!/usr/bin/python

import csv
import json
import os

import copy
import random
import numpy as np
from scipy import spatial
import itertools

class my_point(object):
    def __init__(self,_coord,_num):
        self.coord = _coord
        self.num = _num

def main():

    # Run through each image, create consensus.
    # Evaluate each user on consensus > 2, aka one level above union
    # Remove bottom 10% by creating list and if-statements

    # repeat?

    first = True
    
    with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json",'r') as in_file:
        conv_data = json.load(in_file)

    rejects = []
    random_rejects = []

    full_length = None
        
    for i in range(10):

        agree_dat = []
        gold_perf = []
        indiv_gold = []

        random_gold = []
        random_entries = []
        indiv_rand = []
        
        for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):

            subj_entry = []
            random_entry = []

            anno_files = os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir)

            if len(anno_files) < 2:
                continue
            
            for anno_file in anno_files:

                if anno_file in rejects:
                    continue
                
                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                    user_id = in_file.next().strip('\n')
                    in_file.next()
                    yes_no = in_file.next().strip('\n')
                    
                    reader = csv.reader(in_file,delimiter='\t')
                    entry = []

                    for line in reader:
                        conv_line = map(lambda x: float(x), line)
                        entry.append(my_point(conv_line[0:2],1))
                subj_entry.append([user_id,entry,anno_file,yes_no])  #user_id, annotation, filename, yes/no quality

            for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):

                if anno_file in random_rejects:
                    continue
                
                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                    user_id = in_file.next().strip('\n')
                    in_file.next()
                    yes_no = in_file.next().strip('\n')

                    reader = csv.reader(in_file,delimiter='\t')
                    entry = []

                    for line in reader:
                        conv_line = map(lambda x: float(x), line)
                        entry.append(my_point(conv_line[0:2],1))
                random_entry.append([user_id,entry,anno_file])
                
            if len(subj_entry) != 0:
                
                voted_pts = n_vote(copy.deepcopy(map(lambda x: x[1],subj_entry)),10)  # full vote for gold test at end

                yes_prop = len(filter(lambda x: x[3]=='Yes',subj_entry))*1.0/len(subj_entry)

                if len(subj_entry) > 1:
                    if yes_prop <= 0.5:
                        for k in range(len(subj_entry)):

                            crowd_complement = [subj_entry[j][1] for j in xrange(len(subj_entry)) if j != k]  
                            complement_vote = n_vote(copy.deepcopy(crowd_complement),10) #Vote over complement of single entry
                            complement_coords = map(lambda x: x.coord, complement_vote)

                            entry = subj_entry[k]
                            entry_length = len(entry)
                            agreement = inter_agreement(map(lambda x: x.coord, entry[1]),complement_coords)
                            agree_dat.append([subj_dir,entry[0],entry[2],agreement[0],agreement[1],entry_length]) # subj_id, user_id, anno_file, union, intersection

                    else:

                        for k in range(len(subj_entry)):

                            entry = subj_entry[k]
                            if entry[3] == 'No':
                                agree_dat.append([subj_dir,entry[0],entry[2],0,1,0]) # subj_id, user_id, anno_file, union, intersection
                            else:
                                agree_dat.append([subj_dir,entry[0],entry[2],1,1,0])
                        

                else:
                    # entry = subj_entry[0]
                    # agree_dat.append([subj_dir,entry[0], entry[2],len(entry[1]),len(entry[1])])
                    pass #pass when subj_entry is only one entry, since agreement is impossible to calculate


                try:
                    gold_id = conv_data[subj_dir]
                    gold_data = []

                    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
                        reader = csv.reader(in_file,delimiter='\t')

                        for line in reader:
                            gold_data.append(map(lambda x: float(x), line))

                    full_pts = map(lambda x: x.coord, voted_pts)
                    performance_vec = gold_eval(full_pts, gold_data)

                    gold_entry = [subj_dir]
                    gold_entry.extend(performance_vec)
                    gold_perf.append(gold_entry)

                    for entry in subj_entry:

                        performance_vec = gold_eval(map(lambda x: x.coord,entry[1]),gold_data)

                        gold_entry = [subj_dir,entry[0]]
                        gold_entry.extend(performance_vec) #subj_id, user_id, tp, fp, fn
                        indiv_gold.append(gold_entry)

                except KeyError:
                    pass

            if len(random_entry) != 0:

                voted_pts = n_vote(copy.deepcopy(map(lambda x: x[1],random_entry)),10)
                for entry in random_entry:
                    random_entries.append(entry[2])               # anno_file only

                try:
                    gold_id = conv_data[subj_dir]

                    gold_data = []

                    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
                        reader = csv.reader(in_file,delimiter='\t')

                        for line in reader:
                            gold_data.append(map(lambda x: float(x), line))

                    full_pts = map(lambda x: x.coord, voted_pts)
                    performance_vec = gold_eval(full_pts, gold_data)

                    gold_entry = [subj_dir]
                    gold_entry.extend(performance_vec)
                    random_gold.append(gold_entry)


                    for entry in random_entry:

                        performance_vec = gold_eval(map(lambda x: x.coord,entry[1]),gold_data)

                        gold_entry = [subj_dir,entry[0]]
                        gold_entry.extend(performance_vec) #subj_id, user_id, tp, fp, fn
                        indiv_rand.append(gold_entry)

                except KeyError:
                    pass

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/agreement.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','class_id','intersection','union','anno_len'])
            for line in agree_dat:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/gold_perf.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','tp','fp','fn'])
            for line in gold_perf:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/random_perf.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','tp','fp','fn'])
            for line in random_gold:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/indiv_gold.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','tp','fp','fn'])
            for line in indiv_gold:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/indiv_rand.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','tp','fp','fn'])
            for line in indiv_rand:
                writer.writerow(line)
                
        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rejections.tsv' % (i*10),'w') as out_file:
            for line in rejects:
                out_file.write(line+'\n')

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/rand_rejections.tsv' % (i*10),'w') as out_file:
            for line in random_rejects:
                out_file.write(line+'\n')

        if not first:
            with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/user_thresholding/%02d/user_rejections.tsv' % (i*10),'w') as out_file:
                for line in new_rejects:
                    out_file.write(line+'\n')

        if first:

            user_dict = {}
            agreement_scores = []
            # subj_id, user_id, anno_file, union, intersection : agree_dat

            empty_annos = []
            
            for user_id in set(map(lambda x: x[1], agree_dat)):

                user_annos = filter(lambda x: x[1]==user_id,agree_dat)
                empty_annos.extend(filter(lambda x: x[4] == 0,user_annos))

                #user_annos = filter(lambda x: x[4] != 0, user_annos)
                # union_size = map(lambda x: x[3],user_annos)
                # intersection_size = map(lambda x: x[4], user_annos)

                indiv_agreement = map(lambda x: 1.0*x[3]/x[4],user_annos)
                anno_lengths = map(lambda x: x[5],user_annos)
                mean_length = sum(anno_lengths)*1.0/len(anno_lengths)

                # indiv_agreement = map(lambda x: first_divide(union_size[x],intersection_size[x]),xrange(len(union_size)))
                # indiv_agreement = filter(lambda x: x != 2.0, indiv_agreement)

                if len(indiv_agreement) > 0:
                    agreement_scores.append([user_id,sum(indiv_agreement)/len(indiv_agreement),mean_length])
                else:
                    # agreement_scores.append([user_id,random.random()])
                    pass
                    
                
                user_dict[user_id] = map(lambda x: x[2], user_annos)

                #agreement_scores.append([user_id,first_divide(x[3],x[4])])
            
            # agreement_scores = map(lambda x: [x[2],first_divide(x[3],x[4])], agree_dat) # anno_file, agreement
            
            # empty_annos = filter(lambda x: x[1] == 2.0, agreement_scores)
            # non_empty_annos = filter(lambda x: x[1] != 2.0, agreement_scores)
        
            # sorted_scores = sorted(non_empty_annos, key=lambda x: x[1])

            sorted_scores = sorted(agreement_scores, key=lambda x: (x[1],x[2]))

            # rejects.extend(map(lambda x: x[2],empty_annos))
            # random_rejects.extend(map(lambda x: x[2], empty_annos))

            full_length = len(sorted_scores) / 10

            new_rejects = map(lambda x: x[0],sorted_scores[0:full_length])
            new_reject_files = map(lambda x: user_dict[x],new_rejects)
            new_reject_files = [x for sub in new_reject_files for x in sub]
            
            random.shuffle(sorted_scores)
            new_randoms = map(lambda x: x[0],sorted_scores[0:full_length])
            new_random_files = map(lambda x: user_dict[x],new_randoms)
            new_random_files = [x for sub in new_random_files for x in sub]
            

            rejects.extend(new_reject_files)
            random_rejects.extend(new_random_files)
            
            first = False

        else:

            # agreement_scores = map(lambda x: [x[2],robust_divide(x[3],x[4])], agree_dat)

            agreement_scores = []
            # subj_id, user_id, anno_file, union, intersection : agree_dat

            for user_id in set(map(lambda x: x[1], agree_dat)):
                user_annos = filter(lambda x: x[1]==user_id,agree_dat)

                #user_annos = filter(lambda x: x[4] != 0, user_annos)
                union_size = map(lambda x: x[3],user_annos)
                intersection_size = map(lambda x: x[4], user_annos)

                indiv_agreement = map(lambda x: robust_divide(union_size[x],intersection_size[x]),xrange(len(union_size)))

                anno_lengths = map(lambda x: x[5],user_annos)
                mean_length = sum(anno_lengths)*1.0/len(anno_lengths)

                #indiv_agreement = map(lambda x: union_size[x]*1.0/intersection_size[x],xrange(len(union_size)))

                if len(indiv_agreement) > 0:
                    agreement_scores.append([user_id,sum(indiv_agreement)/len(indiv_agreement),mean_length])
                else:
                    # agreement_scores.append([user_id,random.random()])
                    pass

            sorted_scores = sorted(agreement_scores, key=lambda x: (x[1],x[2]))

            random.shuffle(random_entries)

            # new_rejects = map(lambda x: x[0],sorted_scores[0:full_length])
            # new_randoms = map(lambda x: x,random_entries[0:full_length])

            # rejects.extend(new_rejects)
            # random_rejects.extend(new_randoms)

            new_rejects = map(lambda x: x[0],sorted_scores[0:full_length])
            new_reject_files = map(lambda x: user_dict[x],new_rejects)
            new_reject_files = [x for sub in new_reject_files for x in sub]
            
            random.shuffle(sorted_scores)
            new_randoms = map(lambda x: x[0],sorted_scores[0:full_length])
            new_random_files = map(lambda x: user_dict[x],new_randoms)
            new_random_files = [x for sub in new_random_files for x in sub]

            rejects.extend(new_reject_files)
            random_rejects.extend(new_random_files)            
            
    return


def backup_main():

    # Run through each image, create consensus.
    # Evaluate each user on consensus > 2, aka one level above union
    # Remove bottom 10% by creating list and if-statements

    # repeat?

    first = True
    
    with open("/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/data/gold_conv.json",'r') as in_file:
        conv_data = json.load(in_file)

    rejects = []
    random_rejects = []

    full_length = None
        
    for i in range(10):

        agree_dat = []
        gold_perf = []
        indiv_gold = []

        random_gold = []
        random_entries = []
        indiv_rand = []
        
        for subj_dir in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/'):

            subj_entry = []
            random_entry = []

            anno_files = os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir)

            if len(anno_files) < 2:
                continue
            
            for anno_file in anno_files:

                if anno_file in rejects:
                    continue
                
                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                    user_id = in_file.next().strip('\n')
                    in_file.next()
                    yes_no = in_file.next().strip('\n')
                    
                    reader = csv.reader(in_file,delimiter='\t')
                    entry = []

                    for line in reader:
                        conv_line = map(lambda x: float(x), line)
                        entry.append(my_point(conv_line[0:2],1))
                subj_entry.append([user_id,entry,anno_file,yes_no])  #user_id, annotation, filename, yes/no quality

            for anno_file in os.listdir('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/' % subj_dir):

                if anno_file in random_rejects:
                    continue
                
                with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/annotations/user_annos/%s/%s' % (subj_dir,anno_file), 'r') as in_file:
                    user_id = in_file.next().strip('\n')
                    in_file.next()
                    yes_no = in_file.next().strip('\n')

                    reader = csv.reader(in_file,delimiter='\t')
                    entry = []

                    for line in reader:
                        conv_line = map(lambda x: float(x), line)
                        entry.append(my_point(conv_line[0:2],1))
                random_entry.append([user_id,entry,anno_file])
                
            if len(subj_entry) != 0:
                
                voted_pts = n_vote(copy.deepcopy(map(lambda x: x[1],subj_entry)),10)  # full vote for gold test at end

                yes_prop = len(filter(lambda x: x[3]=='Yes',subj_entry))*1.0/len(subj_entry)

                if len(subj_entry) > 1:
                    if yes_prop <= 0.5:
                        for k in range(len(subj_entry)):

                            crowd_complement = [subj_entry[j][1] for j in xrange(len(subj_entry)) if j != k]  
                            complement_vote = n_vote(copy.deepcopy(crowd_complement),10) #Vote over complement of single entry
                            complement_coords = map(lambda x: x.coord, complement_vote)

                            entry = subj_entry[k]
                            agreement = inter_agreement(map(lambda x: x.coord, entry[1]),complement_coords)
                            agree_dat.append([subj_dir,entry[0],entry[2],agreement[0],agreement[1],len(entry)]) # subj_id, user_id, anno_file, union, intersection

                    else:

                        for k in range(len(subj_entry)):

                            entry = subj_entry[k]
                            if entry[3] == 'No':
                                agree_dat.append([subj_dir,entry[0],entry[2],0,1,0]) # subj_id, user_id, anno_file, union, intersection
                            else:
                                agree_dat.append([subj_dir,entry[0],entry[2],1,1,0])
                        

                else:
                    # entry = subj_entry[0]
                    # agree_dat.append([subj_dir,entry[0], entry[2],len(entry[1]),len(entry[1])])
                    pass #pass when subj_entry is only one entry, since agreement is impossible to calculate


                try:
                    gold_id = conv_data[subj_dir]
                    gold_data = []

                    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
                        reader = csv.reader(in_file,delimiter='\t')

                        for line in reader:
                            gold_data.append(map(lambda x: float(x), line))

                    full_pts = map(lambda x: x.coord, voted_pts)
                    performance_vec = gold_eval(full_pts, gold_data)

                    gold_entry = [subj_dir]
                    gold_entry.extend(performance_vec)
                    gold_perf.append(gold_entry)

                    for entry in subj_entry:

                        performance_vec = gold_eval(map(lambda x: x.coord,entry[1]),gold_data)

                        gold_entry = [subj_dir,entry[0]]
                        gold_entry.extend(performance_vec) #subj_id, user_id, tp, fp, fn
                        indiv_gold.append(gold_entry)

                except KeyError:
                    pass

            if len(random_entry) != 0:

                voted_pts = n_vote(copy.deepcopy(map(lambda x: x[1],random_entry)),10)
                for entry in random_entry:
                    random_entries.append(entry[2])               # anno_file only

                try:
                    gold_id = conv_data[subj_dir]

                    gold_data = []

                    with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/gold_data/voted_data/gold_%s.tsv' % gold_id,'r') as in_file:
                        reader = csv.reader(in_file,delimiter='\t')

                        for line in reader:
                            gold_data.append(map(lambda x: float(x), line))

                    full_pts = map(lambda x: x.coord, voted_pts)
                    performance_vec = gold_eval(full_pts, gold_data)

                    gold_entry = [subj_dir]
                    gold_entry.extend(performance_vec)
                    random_gold.append(gold_entry)


                    for entry in random_entry:

                        performance_vec = gold_eval(map(lambda x: x.coord,entry[1]),gold_data)

                        gold_entry = [subj_dir,entry[0]]
                        gold_entry.extend(performance_vec) #subj_id, user_id, tp, fp, fn
                        indiv_rand.append(gold_entry)

                except KeyError:
                    pass

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/agreement.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','class_id','intersection','union','anno_len'])
            for line in agree_dat:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/gold_perf.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','tp','fp','fn'])
            for line in gold_perf:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/random_perf.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','tp','fp','fn'])
            for line in random_gold:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/indiv_gold.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','tp','fp','fn'])
            for line in indiv_gold:
                writer.writerow(line)

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/indiv_rand.tsv' % (i*10),'w') as out_file:
            writer = csv.writer(out_file,delimiter='\t')
            writer.writerow(['subj_id','user_id','tp','fp','fn'])
            for line in indiv_rand:
                writer.writerow(line)
                
        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rejections.tsv' % (i*10),'w') as out_file:
            for line in rejects:
                out_file.write(line+'\n')

        with open('/Users/Jake/Documents/Projects/crowdProj/MicroscopyMasters/zooniverse/analysis/agreement_thresholding/indiv_annos/%02d/rand_rejections.tsv' % (i*10),'w') as out_file:
            for line in random_rejects:
                out_file.write(line+'\n')

                
        if first:

            # subj_id, user_id, anno_file, union, intersection : agree_dat

            agreement_scores = map(lambda x: [x[2],first_divide(x[3],x[4]),x[5]], agree_dat) # anno_file, agreement
            
            empty_annos = filter(lambda x: x[1] == 2.0, agreement_scores)
            non_empty_annos = filter(lambda x: x[1] != 2.0, agreement_scores)
        
            sorted_scores = sorted(non_empty_annos, key=lambda x: (x[1],x[2]))

            rejects.extend(map(lambda x: x[2],empty_annos))
            random_rejects.extend(map(lambda x: x[2], empty_annos))

            full_length = len(sorted_scores) / 10

            new_rejects = map(lambda x: x[0],sorted_scores[0:full_length])
            
            random.shuffle(sorted_scores)
            new_randoms = map(lambda x: x[0],sorted_scores[0:full_length])
            
            rejects.extend(new_rejects)
            random_rejects.extend(new_randoms)

            first = False

        else:

            agreement_scores = map(lambda x: [x[2],robust_divide(x[3],x[4]),x[5]], agree_dat)

            # subj_id, user_id, anno_file, union, intersection : agree_dat

            sorted_scores = sorted(agreement_scores, key=lambda x: (x[1],x[2]))

            random.shuffle(random_entries)

            new_rejects = map(lambda x: x[0],sorted_scores[0:full_length])
            new_randoms = map(lambda x: x,random_entries[0:full_length])

            rejects.extend(new_rejects)
            random_rejects.extend(new_randoms)
            
    return


def first_divide(x,y):
    try:
        result = x*1.0/y
    except ZeroDivisionError:
        result = 2.0

    return result

def robust_divide(x,y):
    try:
        result = x*1.0/y
    except ZeroDivisionError:
        #result = random.random()
        result = 1.0

    return result

# change to work with point objects

def inter_agreement(datapts,goldpts):
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (1,1)
    elif gold_n == 0:
        return (0,exp_n)
    elif exp_n == 0:
        return (0,gold_n)
        
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
            
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    matches = len(filter(lambda x: x <= 25,dist_mins))
        
    tp = matches

    return (tp,exp_n+gold_n)


def gold_eval(datapts,goldpts):
    
    tp = 0
    fn = 0
    fp = 0

    totes = 0
    
    gold_n = len(goldpts)
    exp_n = len(datapts)

    if gold_n == 0 and exp_n == 0:
        return (0,0,0)
    elif gold_n == 0:
        return (0,exp_n,0)
    elif exp_n == 0:
        return (0,0,gold_n)
        
    if gold_n < exp_n:
        dist_mat = spatial.distance_matrix(goldpts,datapts)
    else:
        dist_mat = spatial.distance_matrix(datapts,goldpts)

    dist_mins = np.amin(dist_mat,axis=1)
            
    #matches = len(filter(lambda x: x <= 25,dist_mins))
    matches = len(filter(lambda x: x <= 10,dist_mins))
    
    tp += matches
        
    fp += (exp_n-matches)

    fn += (gold_n - matches)

    return (tp,fp,fn)

def find_matches(mat,fewer_rows,dist_thresh):
    #rows
    row_mins = []
    for i in xrange(mat.shape[0]):
        inds = np.where(mat[i,:] == min(mat[i,:]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        row_mins.append(ind)

    col_mins = []
    for i in xrange(mat.shape[1]):
        inds = np.where(mat[:,i] == min(mat[:,i]))[0].tolist()
        if len(inds) > 1: #ignore for now....
            ind = 0
        else:
            ind = inds[0]
        col_mins.append(ind)


    matches = []

    if fewer_rows:
        for i in xrange(len(row_mins)):
            col_ind = row_mins[i]
            if mat[i,col_ind] < dist_thresh:
                matches.append((i,col_ind,mat[i,col_ind]))
    else:
        for i in xrange(len(col_mins)):
            row_ind = col_mins[i]
            if mat[row_ind,i] < dist_thresh:
                matches.append((row_ind,i,mat[row_ind,i]))

    return sorted(matches,key= lambda x: x[2])

def two_vote(points1,points2,dist_thresh):

    results = []
    
    n = len(points1)
    m = len(points2)

    diff = n-m
        
    if n == 0 or m == 0:
        results = []
        results.extend(points1)
        results.extend(points2)
        return results

    dist_mat = spatial.distance_matrix([pt.coord for pt in points1],[pt.coord for pt in points2])
    
    points = find_matches(dist_mat,(n<m),dist_thresh)

    for point in points:

        point1 = points1[point[0]]
        point2 = points2[point[1]]
        
        points1[point[0]] = None
        points2[point[1]] = None

        if point1 == None:
            results.append(point2)
        elif point2 == None:
            results.append(point1)
        else:
            avg_point = [(point1.coord[0]+point2.coord[0])/2,(point1.coord[1]+point2.coord[1])/2]
        
            results.append(my_point(avg_point,point1.num+point2.num))
        

    f_pts1 = filter(lambda x: x is not None,points1)
    f_pts2 = filter(lambda x: x is not None,points2)

    results.extend(f_pts1)
    results.extend(f_pts2)

    return results

def n_vote(expert_data,dist_thresh):

    random.shuffle(expert_data)
        
    first_exp = expert_data.pop()

    while expert_data:
        next_exp = expert_data.pop()
        join_exp = two_vote(first_exp, next_exp,dist_thresh)
        first_exp = join_exp
            
    return first_exp


if __name__ == "__main__":
    main()
    backup_main()
